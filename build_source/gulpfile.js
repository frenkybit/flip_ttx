const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const sourcemaps = require('gulp-sourcemaps')
const babel = require('gulp-babel')
const rename = require('gulp-rename')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const sass = require('gulp-sass')
const shell = require('gulp-shell')
const plumber = require('gulp-plumber')
const notify = require('gulp-notify')
const cleanCSS = require('gulp-clean-css');


var styles = 'src/css/';
var scripts ='src/js/';

var webserver = require('gulp-webserver');
 
gulp.task('webserver', function() {
  gulp.src('./build/')
    .pipe(gulp.dest('./'))
    .pipe(webserver({
      livereload: false,
      directoryListing: false,
      open: true,
      host: '0.0.0.0',
      // fallback: 'index.html'
    }))
});




// gulp.task('server', function () {
//   browserSync.init({
//     proxy: 'https://bibi.local:9002/'
//   })
// })
// Следит за изменением файлов и запускает пересборки и livereload

gulp.task('watch', function () {
  gulp.watch('src/**/*.scss', ['sass'])
  gulp.watch('src/js/app/**/*.js', ['scripts'])
  gulp.watch('src/*.html', ['html'])
  gulp.watch('src/img/**/*.*', ['images'])
})


// Собирает один минифицированный JS файл
gulp.task('scripts', function () {
  return gulp.src('src/js/app/**/*.js')
    .pipe(plumber({errorHandler: notify.onError("main build error: <%= error.message %>")}))
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015'],
    }))
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'))
})


gulp.task('sass', function () {
  return gulp.src('src/css/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber({errorHandler: notify.onError("sass error: <%= error.message %>")}))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'));
})

gulp.task('minify-css', function() {
  return gulp.src('build/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('build/css/min'));
});

gulp.task('vendor', function () {
  return gulp.src([
    'src/js/vendor/*.js'
  ])
    .pipe(plumber({errorHandler: notify.onError("box build error: <%= error.message %>")}))
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest('build/js'))
})


// Копирует шрифты
gulp.task('fonts', function () {
  return gulp.src('src/fonts/**')
    .pipe(gulp.dest('build/fonts'))
})

// Копирует шрифты
gulp.task('html', function () {
  return gulp.src('src/*.html')
    .pipe(gulp.dest('build/'))
})

// Копирует изображения
gulp.task('images', function () {
  return gulp.src('src/img/**')
    .pipe(gulp.dest('build/img'))
})


gulp.task('build', ['html', 'scripts', 'sass', 'vendor', 'fonts', 'images'])
gulp.task('minify', ['minify-css'])
gulp.task('server', ['watch', 'webserver'])

// стандартная таска для gulp
gulp.task('default', ['build'])
