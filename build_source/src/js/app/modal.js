$.extend(true, $.magnificPopup.defaults, {
	tClose: 'Закрыть (Esc)',
	tLoading: 'Загружается...',
		gallery: {
		tPrev: 'Предыдущая (←)',
		tNext: 'Следующая (→)',
		tCounter: '%curr% из %total%'
	},
	image: {
		tError: '<a href="%url%">Данная картинка</a> не может быть загружена.'
	},
	ajax: {
		tError: '<a href="%url%">Содержимое</a> не может быть загружено.'
	},
	closeMarkup: '<div class="modal-close js-mfp-close"></div>',
	removalDelay: 300,
	closeBtnInside: true
});

$(function(){
    var scrollPosition = 0;

    $(document).on('click', '.js-modalLink', function(event) {
        event.preventDefault();

        var src = $(this).data('mfp-src'),
            type = $(this).data('mfp-ajax') || 'inline';

        $.magnificPopup.open({
            items: {
                src: src,
                type: type
            },
            closeBtnInside: true,
            callbacks: {
                open: function() {
                    scrollPosition = $(window).scrollTop();
                    $('body').addClass('mfp-open');
                },
                close: function(){
                    $('body').removeClass('mfp-open');
                    $(window).width() <= 767 && $('body, html').scrollTop(scrollPosition);
                }
            }
        });
    });

    $(document).on('click', '.js-modalClose', function(){
        $.magnificPopup.close();
    });
});