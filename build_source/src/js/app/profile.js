$(function(){
	var profileImagesSwiper = new Swiper('.js-profileImagesSwiper .swiper-container', {
	    speed: 400,
	    slidesPerView: 'auto',
	    spaceBetween: 1,
	    pagination: {
    		el: '.swiper-pagination',
    		clickable: true
    	}
	});


	var profileInfoImagesSwiper = new Swiper('.js-profileInfoImagesSwiper .swiper-container', {
	    speed: 400,
	    slidesPerView: 'auto',
	    spaceBetween: 1
	});


	var profileInfoTagSwiper = new Swiper('.js-profileInfoTagSwiper .swiper-container', {
	    speed: 400,
	    slidesPerView: 'auto',
	    spaceBetween: 8
	});

	var profileViewSwiper = new Swiper('.js-profileViewSwiper .swiper-container', {
	    speed: 400,
	    slidesPerView: 'auto',
	    spaceBetween: 8
	});


	$(window).on('scroll', function(){
		profileStickyButtonScroll();
		if($('.js-profileProgressSticky').length) profileProgressStickyScroll();
	});

	profileStickyButtonScroll();
	if($('.js-profileProgressSticky').length) profileProgressStickyScroll();


	function profileStickyButtonScroll(){
		var scrolled = $(window).scrollTop();
		var profileBlock = $('.js-profile');
		var profileOffset = profileBlock.outerHeight() + profileBlock.offset().top - $(window).height();
		var profileStickyButton = $('.js-profileStickyButton');

		if(scrolled > profileOffset){
			profileStickyButton.removeClass('is-sticky');
		}
		else {
			profileStickyButton.addClass('is-sticky');
		}
	}

	if($('.js-profileProgressSticky').length){
		var profileProgressClone = $('.js-profileProgressSticky').clone();

		$('.js-profileProgressSticky').prepend(profileProgressClone.removeClass('js-profileProgressSticky').addClass('js-profileProgressStickyClone is-sticky'));		
	}

	function profileProgressStickyScroll(){
		var scrolled = $(window).scrollTop();
		var profileProgressBlock = $('.js-profileProgressSticky');
		var profileProgressBlockOffset = profileProgressBlock.offset().top + profileProgressBlock.outerHeight();
		var profileProgressStickyClone = $('.js-profileProgressStickyClone');

		if(scrolled < profileProgressBlockOffset){
			profileProgressStickyClone.removeClass('is-show');
		}
		else {
			profileProgressStickyClone.addClass('is-show');
		}
	}
});