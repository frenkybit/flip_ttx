$(function(){
	var body = $('body'),
		scrollPosition = 0,
		filterAside = $('.js-filterAside');

	$('.js-rangeSlider').ionRangeSlider({
		type: 'double',
		min: $(this).data('min'),
		max: $(this).data('max'),
		from: $(this).data('from'),
		to: $(this).data('to'),
		force_edges: true,
		hide_from_to: true,
		hide_min_max: true
	}); 

	$(document).on('click', '.js-filterBurger', function(){
		if(filterAside.hasClass('is-open')){
			body.removeClass('is-filter-open');
			filterAside.removeClass('is-open');
			$('body, html').scrollTop(scrollPosition);
		}
		else {
			scrollPosition = $(window).scrollTop();
			body.addClass('is-filter-open');
			filterAside.addClass('is-open');
		}
	});

	$(document).on('click', '.js-filterAsideClose', function(){
		body.removeClass('is-filter-open');
		filterAside.removeClass('is-open');
		$('body, html').scrollTop(scrollPosition);
	});
});