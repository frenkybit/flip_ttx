$(function(){
	var authModal = $('#authModal');

	$(document).on('click', '.js-authModalLink', function(event){
		event.stopPropagation();
		event.preventDefault();
		authModal.addClass('is-open');
	});

	$(document).on('click', '.js-authModalClose', function(){
		authModal.removeClass('is-open');
	});

	$(document).on('click', function(event){
		if(authModal.has(event.target).length === 0){
		    authModal.removeClass('is-open');
		}
	});
});