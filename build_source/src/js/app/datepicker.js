$(function(){
	$('.js-datepickerRange').datepicker({
		range: true,
		multipleDatesSeparator: ' - ',
		autoClose: true
	});

	$('.js-datepickerSingle').datepicker({
		autoClose: true
	});
});