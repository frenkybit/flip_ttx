$(function(){
	function chatMessageContainerHeight(){
		var screenHeight = $(window).height() - $('.ftt-minheader').height();

		$('.js-chatMessageContainer').css({
			'height': screenHeight
		});
	}

	$(window).width() >= 640 && chatMessageContainerHeight();

	$(window).on('resize', function(){
		$(window).width() >= 640 && chatMessageContainerHeight();
	});


	function chatScrollToBottom(){
		if($('.ftt-chat-message__scroll').length > 0) $('.ftt-chat-message__scroll').scrollTop($('.ftt-chat-message__scroll-container').height());
	}

	chatScrollToBottom();

	$(window).on('load', function(){
		chatScrollToBottom();
	});

	var body = $('body');
	var scrollPos = 0;

	// Mobile: отобразить блок сообщений
	$(document).on('click', '.js-chatInterlocutorElem', function(event){
		event.preventDefault();
		scrollPos = $(window).scrollTop();
		body.addClass('is-chat-open');
	});

	// Mobile: скрыть блок сообщений
	$(document).on('click', '.js-chatMessageClose', function(event){
		event.preventDefault();
		body.removeClass('is-chat-open');
		$('body, html').scrollTop(scrollPos);
	});
});
