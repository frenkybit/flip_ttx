﻿var scrollPosition = 0;
var body = $('body');

function salt() {
	if (CURRENT_USER != null && CURRENT_USER != undefined) {
		return "&lang="+FLang["key"]+"&salt="+CURRENT_USER["salt"]+"&r=" + (Math.floor(Math.random() * 100000));	
	}
	return '';
}

function cuid() {
	if (CURRENT_USER != null && CURRENT_USER != undefined) {
		return CURRENT_USER["id"];	
	}
	return 0;
}

function init() {
	if (INTERFACE === null || INTERFACE === undefined) {
		return '';
	}
	if (INTERFACE["tab"] === "search") {
		init_search();
	} else if (INTERFACE["tab"] === "feed") {
		if (cuid() == 5082896) {
			init_feed_new();
		} else {
			init_feed();
		}
	} else if (INTERFACE["tab"] === "settings") {
		init_settings();
	} else if (INTERFACE["tab"] === "trips") {
		init_trips();
	} else if (INTERFACE["tab"] === "chat") {
		init_chat();
		$(window).width() >= 640 && chatMessageContainerHeight();
		chatScrollToBottom();
		
		$(window).on('resize', function(){
			$(window).width() >= 640 && chatMessageContainerHeight();
		});
		
		$(window).on('load', function(){
			chatScrollToBottom();
		});
	} else if (INTERFACE["tab"] === "profile") { 
		init_profile();
	} else if (INTERFACE["tab"] === "thanks") { 
		init_thanks();
	} else if (INTERFACE["tab"] === "edit") { 
		init_edit();
	} else if (INTERFACE["tab"] === "tip") { 
		init_tip();
	}
		
	$(document).on('click', '.js-mobmenuBurger', function(){
		$('.ftt-mobmenu ').addClass('is-open');
	});

	$(document).on('click', '.js-mobmenuClose', function(){
		$('.ftt-mobmenu ').removeClass('is-open');
	});
}

/*function init_feed(cid) {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden');
}*/


function profileStickyButtonScroll(){
	var scrolled = $(window).scrollTop();
	var profileBlock = $('.js-profile');
	var profileOffset = profileBlock.outerHeight() + profileBlock.offset().top - $(window).height();
	var profileStickyButton = $('.js-profileStickyButton');

	if(scrolled > profileOffset){
		profileStickyButton.removeClass('is-sticky');
	}
	else {
		profileStickyButton.addClass('is-sticky');
	}
}


function profile_languages(data){
	var la = [];
	for ( i = 0; i < data.length; i++ ){
		la.push(data[i]["val"]);
	}
	return la.join(', ');
}

function img_html(img) {
	return '<div class="swiper-slide">\
				<div class="ftt-profile__image">\
					<a href="'+img+'" data-fancybox="profile_group">\
						<img src="'+img+'" alt="">\
					</a>\
				</div>\
			</div>';
}
function img_insta_html(img, insta) {
	return '<div class="swiper-slide">\
				<a href="'+img.url+'" data-caption="'+'http://instagram.com/'+insta+'" data-fancybox="inst_group" class="ftt-profile__info-image" style="background-image: url(\''+img.thumb+'\')"></a>\
			</div>';
}

function string_cons(cons, sex){
	if (cons === undefined) { cons = 0 }
	var val = cons.toString().substring(cons.length - 1);	
	
	if (val == 1) {
		if (cons == 11) {return FLang["Stran_"+sex];}
		return FLang["Stranu_"+sex];
	} else if (val === 2 || val === 3 || val === 4) {
		if (cons < 15 && cons > 11) {return FLang["Stran_"+sex];}
		return FLang["Strani_"+sex];
	} else {
		return FLang["Stran_"+sex];
	}
}

function string_days(cons){
	if (cons === undefined) { cons = 0 }
	var val = cons.toString().substring(cons.length - 1);	
	
	if (val == 1) {
		if (cons == 11) {return FLang["Dney_with"];}
		return FLang["Den_with"];
	} else if (val === 2 || val === 3 || val === 4) {
		if (cons < 15 && cons > 11) {return FLang["Dney_with"];}
		return FLang["Dnya_with"];
	} else {
		return FLang["Dney_with"];
	}
}

function string_tips(cons){
	if (cons === undefined) { cons = 0 }
	var val = cons.toString().substring(cons.length - 1);	
	
	if (val == 1) {
		if (cons == 11) {return FLang["Sovetov"];}
		return FLang["Sovet"];
	} else if (val === 2 || val === 3 || val === 4) {
		if (cons < 15 && cons > 11) {return FLang["Sovetov"];}
		return FLang["Soveta"];
	} else {
		return FLang["Sovetov"];
	}
}

function string_comment(cons){
	if (cons === undefined) { cons = 0 }
	var val = cons.toString().substring(cons.length - 1);	
	
	if (val == 1) {
		if (cons == 11) {return FLang["сommentov"];}
		return FLang["сomment"];
	} else if (val === 2 || val === 3 || val === 4) {
		if (cons < 15 && cons > 11) {return FLang["сommentov"];}
		return FLang["сommenta"];
	} else {
		return FLang["сommentov"];
	}
}

function string_ans(cons){
	if (cons === undefined) { cons = 0 }
	var val = cons.toString().substring(cons.length - 1);	
	
	if (val == 1) {
		if (cons == 11) {return FLang["answerov"];}
		return FLang["answer"];
	} else if (val === 2 || val === 3 || val === 4) {
		if (cons < 15 && cons > 11) {return FLang["answerov"];}
		return FLang["answera"];
	} else {
		return FLang["answerov"];
	}
}

function make_soc_share(soc, url, icon){
	surl = 'https://vkontakte.ru/share.php?url=';
	ic = '';
	cla = ' class="ftt-trip-el__share"';
	if (icon != undefined && icon){
		ic = '<img src="/img/icons/'+soc+'.svg">';
		cla = ''; 
	}
	
	if (soc === "vk") {
		surl = 'https://vkontakte.ru/share.php?url=';
	}
	if (soc === "fb") {
		surl = 'https://www.facebook.com/sharer.php?u=';
	}
	
	return '<a'+cla+' onclick="window.open(this.href, \''+FLang["Share_"+soc]+'\', \'width=800,height=300\');return false" href="'+surl+url+'" title="'+FLang["Share_"+soc]+'">'+ic+'</a>'
}

function init_trips() {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden').remove();
	$("#menu_trips").addClass('is-active');
	$.getJSON("/api/v2/listmy?"+salt(),
		function(data){
			if (data.status && data.status == 200) {
				txt = '<div class="ftt-page">\
						<div class="ftt-page__head">\
							<div class="ftt-page__title">'+FLang["Trips"]+'</div>\
						</div>\
						<div class="ftt-page__wrap">\
						    <div class="ftt-page__wrap-button">\
								<div class="ftt-button style_light_orange js-modalLink" data-mfp-src="#marksModal">'+FLang["Add_trip"]+'</div>\
							</div>\
							<div class="ftt-trips">\
								<div class="ftt-trips__grid">\
									<div class="ftt-trips__col">';
									var limit = parseInt(data.res.length / 2);
									var r1 = new RegExp("[ -]","ig");
									var r2 = new RegExp("[,]","ig");

									for ( i = 0; i < data.res.length; i++ ){
										city = data.res[i]["city"];
										if ( data.res[i]["city"] != data.res[i]["country"] ) {
											city += ', '+data.res[i]["country"];
										}
										
										var newusers = parseInt(data.res[i]["new"]);
										nu_class = '';
										nu_span = '';
										if (newusers > 0){
											nu_class = ' is-yellow';
											nu_span = '<span class="ftt-trip-el__new-label">'+FLang["New_users_Search"]+'</span>';
										}
										
										var url = make_url(city, r1, r2) + data.res[i]["markid"]+"/";
										txt +='<div class="ftt-trip-el'+nu_class+'" id="mark_'+data.res[i]["markid"]+'">\
											<a href="'+url+'" class="ftt-trip-el__image"></a>\
											'+nu_span+'<a href="'+url+'" class="ftt-trip-el__title">'+city+'</a>\
											<div class="ftt-trip-el__date">'+data.res[i]["date"]+'</div>\
											'+make_soc_share(data.soc, 'https://flipthetrip.com/trip/'+data.res[i]["markid"]+'/'+FLang["key"]+'/')+'\
												<div class="ftt-trip-el__setting js-dropdownToggle">\
												<div class="ftt-trip-el__setting-control js-dropdownToggleControl"><div class="ftt-trip-el__setting-dot"></div></div>\
												<div class="ftt-trip-el__setting-dropdown">\
													<div class="ftt-trip-el__setting-el" onclick="del_mark('+data.res[i]["markid"]+', this);return false;">'+FLang["Delete"]+'</div>\
												</div>\
											</div>\
										</div>';
										
										if (i > 0 && i % limit === 0 && data.res[i+1] !== undefined) {
											txt += '</div><div class="ftt-trips__col">';
										}
									}

				txt+='				</div>\
								</div>\
							</div>\
						</div>\
					</div>';

				$("#container").html(txt).removeClass('loadb');
				$(document).on('click', '.js-dropdownToggleControl', function(event){
					event.stopPropagation();
					$(this).parent().toggleClass('is-open');
					return false;
				});

				$(document).on('click', function(event){
					$('.js-dropdownToggle').removeClass('is-open');
				});
				
							
				$(document).on('click', '.js-modalLink', function(event) {
					$("#marksModal button").html(FLang["Add_trip"]);
					$("#marksModal div.ftt-modal-tips-body p").html(FLang["My_trip_intro"]);
					$("#marksModal div.ftt-modal-tips-mobheader__title, #marksModal div.ftt-modal-tips-title").html(FLang["My_trip"]);
					$("#marksModal div.ftt-input-date input").attr('placeholder', FLang["Select_dates"]);
					if ( ! $("#marksModal div.ftt-modal-tips-location").hasClass('hidden') ) { $("#marksModal div.ftt-modal-tips-location").addClass("hidden"); }
					$("#searchmark").removeClass("hidden");
					
					event.preventDefault();

					var src = $(this).data('mfp-src'),
						type = $(this).data('mfp-ajax') || 'inline';

					$.magnificPopup.open({
						items: {
							src: src,
							type: type
						},
						closeBtnInside: true,
						callbacks: {
							open: function() {
								scrollPosition = $(window).scrollTop();
								$('body').addClass('mfp-open');
							},
							close: function(){
								$('body').removeClass('mfp-open');
								$(window).width() <= 767 && $('body, html').scrollTop(scrollPosition);
							}
						}
					});
				});

				$(document).on('click', '.js-modalClose', function(){
					$.magnificPopup.close();
				});
				
				$('.js-datepickerRange').datepicker({
					range: true,
					multipleDatesSeparator: ' - ',
					autoClose: true,
					language: FLang["key"],
					onSelect: function(formattedDate, date) {
						if (date.length === 2){
							$("#mark_from").val((date[0]).toISOString().substring(0, 10));
							$("#mark_to").val((date[1]).toISOString().substring(0, 10));
						}
					}
				});
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function init_tip() {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden').remove();
	
	$.getJSON("/api/v2/showqst?url="+INTERFACE["qkey"]+salt(),
		function(data){
			if (data.status && data.status == 200) {
			
				var city = data.qst["city"];
				if ( data.qst["city"] != data.qst["country"] ) {
					city += ', '+data.qst["country"];
				}
			
				r = new RegExp("\n","ig")
				txt = '	<div class="ftt-page ftt-page-tip">\
							<div class="ftt-page__head">\
								<div class="ftt-page__title">'+city+'</div>\
							</div>\
							<div class="ftt-page__wrap">\
								<div class="ftt-tip-detail">'+draw_tip(data.qst, r, 0, 0)+'\
								</div>\
							</div>\
						</div>';

				$("#container").html(txt).removeClass('loadb');
				$("div.ftt-tip-detail div.ftt-tile__text").css("max-height", "none").attr('onclick','').unbind('click'); 
				$("div.ftt-tip-detail div.ftt-tile__date").css("max-height", "none").attr('onclick','').unbind('click'); 
					
				$(document).on('click', '.js-dropdownToggleControl', function(event){
					event.stopPropagation();
					$(this).parent().toggleClass('is-open');
					return false;
				});

				$(document).on('click', function(event){
					$('.js-dropdownToggle').removeClass('is-open');
				});
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function init_edit() {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden').remove();
	
	$.getJSON("/api/v2/profile?uid="+cuid()+salt(),
		function(data){
			if (data.status && data.status == 200) {
				
				txt = '	<div class="ftt-page ftt-page-profile">\
							<div class="ftt-page__wrap">\
								<div class="ftt-profile js-profile">\
									';
				var hide_upload = '';						
				if (data.profile["photos"].length) {
					if (data.profile["photos"].length >= 5) {
						hide_upload = ' hidden';
					}
					
					txt += '<div class="ftt-profile__images js-profileImagesSwiper">\
								<div class="swiper-container">\
									<div class="swiper-wrapper">';
						
					for ( i = 0; i < data.profile["photos"].length; i++ ){
						txt += '<div class="swiper-slide">\
									<div class="ftt-profile__image">\
										<div class="ftt-profile__image-del" onclick="del_photo('+data.profile["photos"][i][1]+', this);"></div>\
										<div class="ftt-profile__image-select"><div class="ftt-profile__image-select-btn" onclick="make_ava('+data.profile["photos"][i][1]+', this);">'+FLang["Make_avatar"]+'</div></div>\
										<img src="'+data.profile["photos"][i][0]+'" alt="">\
									</div>\
								</div>';
					}
					
					txt+='			</div>\
								</div>\
								<div class="swiper-pagination"></div>\
							</div>';
				}		
				
				var aboutval = '';
				if (data.profile["about"] !== "") {
					aboutval = data.profile["about"];
				}
				var birth = '';
				if (data.profile["birth"] !== "") {
					var s = data.profile["birth"].split('-');
					birth = s[2]+'.'+s[1]+'.'+s[0];
				}
				var city_home = '';
				if (data.profile["city_home"] !== "") {
					city_home = data.profile["city_home"];
				}
									
							txt+='		<div class="ftt-profile__edit'+hide_upload+'" id="add_photo">\
											<form method="post" id="attach" enctype="multipart/form-data">\
											<label class="ftt-button button_file style_border_orange">\
												<input type="file" name="filesToUpload[]" id="filesToUpload">\
												'+FLang["Upload_photo"]+'\
											</label>\
											</form>\
										</div>\
										<div class="ftt-profile__info ftt-profile__info-edit">\
											<div class="ftt-profile__info-el">\
												<div class="ftt-fieldset__label">'+FLang["About"]+' <span id="about"></span></div>\
												<textarea name="" id="" cols="30" rows="10" class="ftt-input" placeholder="" onkeyup="saveabout(this);">'+aboutval+'</textarea>\
											</div>\
											<div class="ftt-profile__info-el">\
												<div class="ftt-fieldset__label">'+FLang["Birth_date"]+'</div>\
												<div class="ftt-input-date">\
													<input type="text" class="ftt-input js-datepickerSingle" placeholder="'+FLang["Birth_date"]+'" value="'+birth+'">\
												</div>\
											</div>\
											<div class="ftt-profile__info-el">\
												<div class="ftt-fieldset__label">'+FLang["Hometown"]+'</div>\
												<div class="ftt-suggest-input">\
													<input type="text" class="ftt-input" placeholder="'+FLang["Hometown_placeholder"]+'" id="hometown" onkeyup="getpan(this, 1);" onfocus="getpan(this, 1);" value="'+city_home+'">\
													<div class="ftt-searchbar__suggest hidden" id="hometown_div"></div>\
												</div>\
											</div>'
										txt += lang_filters_profile(data.profile["langs"]);
									txt+='	<div class="ftt-profile__info-el">\
												<div class="ftt-fieldset__label">'+FLang["Visited_cons"]+'</div>\
												<div class="ftt-suggest-input">\
													<input type="text" class="ftt-input" placeholder="'+FLang["Input_country"]+'" id="country" onkeyup="getpan(this, 1, 1);" onfocus="getpan(this, 1, 1);">\
													<div class="ftt-searchbar__suggest hidden" id="country_div"></div>\
												</div>\
												<div class="ftt-profile__info-tags" id="viscons">';
										for ( i = 0; i < data.profile["countries"].length; i++ ){
											txt +='<div class="ftt-profile__info-tag del-tag" onclick="del_con(this);">'+data.profile["countries"][i]+'</div>'
										}		
										
										checked = '';
										if (data.profile["slink"]) {
											checked = ' checked="checked"';
										}
										
										txt+='	</div>\
											</div>\
											<div class="ftt-profile__info-el">\
												<div class="ftt-profile__info-label">'+FLang["Show_social"]+' '+FLang["Soc_"+data.profile["soc"]]+'</div>\
												<label class="ftt-checkbox-slider">\
													<input onchange="save_soc(this);" type="checkbox"'+checked+'>\
													<span class="ftt-checkbox-slider__icon"></span>\
												</label>\
											</div>\
											<div class="ftt-profile__info-el">\
												<div class="ftt-profile__info-label">'+FLang["Instagram_photos"]+'</div>\
												<div class="ftt-profile__info-button">';
												
										if (data.profile["instagram"]["login"] !== "") {
											txt += '<a href="" class="ftt-button style_light_orange" onclick="removeinsta(this);return false;">'+FLang["Remove_instagram"]+'</a>';
										} else {
											txt += '<a href="" class="ftt-button style_light_orange" onclick="addinsta(this);return false;" id="adins">'+FLang["Add_instagram"]+'</a>';
										}
									txt+='		</div>\
											</div>\
										</div>\
								</div>\
							</div>\
						</div>';

				$("#container").html(txt).removeClass('loadb');
			
				$('#attach').change(function() {
					
					var data = new FormData();
					$.each($('#filesToUpload')[0].files, function(i, file) {
						data.append('file', file);
					});
					
					$.ajax({
						url: '/api/v2/upload?'+salt(),
						data: data,
						cache: false,
						contentType: false,
						processData: false,
						type: 'POST',
						success: function(data){
							if (data.status && data.status == 200) {
								href_url('/edit/');
							} else {
								exeption(data, $("#filesToUpload"));
							}
						}
					});
				});
				
				var profileImagesSwiper = new Swiper('.js-profileImagesSwiper .swiper-container', {
					speed: 400,
					slidesPerView: 'auto',
					spaceBetween: 1,
					pagination: {
						el: '.swiper-pagination',
						clickable: true
					}
				});
				
				$('.js-datepickerSingle').datepicker({
					autoClose: true,
					language: FLang["key"],
					onSelect: function(formattedDate, date, fn) {
						savebirth(formattedDate, fn);
					}
				});
				
				if (data.profile["instagram"]["login"] === "") {
					checkinstatoken();
				}
			
			} else {
				exeption(data, o);
			}
		}
	);
}

function addinsta(o){
	href_url('https://api.instagram.com/oauth/authorize/?client_id=0612e3f43179443baab2215b799295a1&redirect_uri='+encodeURI('https://'+window.location.host+'/edit/')+'&response_type=token')
}

function checkinstatoken(){
	var url = window.location.href;
    var access_token = url.match(/(?:\#access_token\=)([\S\s]+)$/);
	if (access_token && access_token.length) {
		$("#adins").addClass('loadb');
		$.post("/api/v2/instagram", "&ist="+encodeURI(access_token[1])+salt(),
			function(data){
				$("#adins").removeClass('loadb');
				if (data.status && data.status == 200) {
					$('<a href="" class="ftt-button style_light_orange" onclick="removeinsta(this);return false;">'+FLang["Remove_instagram"]+'</a>').insertAfter($("#adins"));
					$("#adins").remove();
				} else {
					exeption(data, $("#adins"));
				}
			}, "json"
		);
	}
}

function del_mark(id, o) {
	$(o).addClass('loadb');
	
	$.post("/api/v2/delf", "&id="+id+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$("#mark_"+id).remove();
			} else {
				exeption(data, o);
			}
		}, "json"
	);
}

function removeinsta(o) {
	$(o).addClass('loadb');
	
	$.post("/api/v2/removeinsta", salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$('<a href="#" class="ftt-button style_light_orange" onclick="addinsta(this);return false;" id="adins">'+FLang["Add_instagram"]+'</a>').insertAfter(o);
				$(o).remove();
			} else {
				exeption(data, o);
			}
		}, "json"
	);
}

function saveabout(o) {
	$(o).addClass('loadb');
	$("#about").html('<b>'+FLang["Saving"]+'</b>')
	$.post("/api/v2/saveabout", "&mes="+encodeURIComponent(o.value)+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$("#about").html('<b>'+FLang["Saved"]+'</b>');
				$("#about b").show().fadeOut(5000,function(){$(this).remove();});
			} else {
				exeption(data, $("#about"));
			}
		}, "json"
	);
}

function save_soc(o){
	var val = o.checked ? 0: 1;
	$(o).addClass('loadb');
	$.getJSON("/api/v2/saveshlink?hidden="+val+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
			} else {
				exeption(data, o);
			}
		}
	);
}

function del_con(o) {
	$(o).addClass('loadb');
	$.getJSON("/api/v2/visited_removebycon?con="+encodeURIComponent($(o).html())+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$(o).remove();
			} else {
				exeption(data, o);
			}
		}
	);
}

function savebirth(val, o) {
	if (! val) {return;}
	var s = val.split('.');
	$(o).addClass('loadb');
	$.getJSON("/api/v2/savebirth?set="+s[2]+'.'+s[1]+'.'+s[0]+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
			} else {
				exeption(data, o);
			}
		}
	);
}

function make_ava(im, o) {
	if (! im) {return;}
	$(o).addClass('loadb');
	
	$.getJSON("/api/v2/makeava?im="+im+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$('span.ftt-minheader__user-avatar').css("background-image", 'url('+data.url+')'); 
			} else {
				exeption(data, o);
			}
		}
	);
}

function save_country(cid, o, val) {
	$(o).addClass('loadb');
	$.getJSON("/api/v2/visited_add?cid="+cid+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$("#country_div").html('').addClass('hidden');
				$("#country").val('');
				$("#viscons").prepend('<div class="ftt-profile__info-tag del-tag" onclick="del_con(this);">'+val+'</div>')
			} else {
				exeption(data, o);
			}
		}
	);
}

function save_hometown(cid, o, val) {
	$(o).addClass('loadb');
	$.getJSON("/api/v2/save_hometown?cid="+cid+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$("#hometown_div").html('').addClass('hidden');
				$("#hometown").val(val);
			} else {
				exeption(data, o);
			}
		}
	);
}

function del_photo(im, o) {
	if (! im) {return;}
	$(o).addClass('loadb');
	
	$.getJSON("/api/v2/delimg?im="+im+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$(o).parent().parent().remove();
				$("#add_photo").removeClass('hidden');
			} else {
				exeption(data, o);
			}
		}
	);
}

function draw_progress(data) {


		var per = parseInt( parseInt(data["strength"]) / 10 );
		txt = '<div class="ftt-profile__progress">\
					<div class="ftt-profile__info-el">\
						<div class="ftt-profile__info-label">'+FLang["Profile_strength"]+': <b class="ftt-profile__progress-percent">'+data["strength"]+'%</b></div>\
						<div class="ftt-profile__progress-bar">';
		
		for ( i=1; i <= 10; i++ ){
			active = '';
			if (i <= per){ active = ' is-active'}
			txt += '<div class="ftt-profile__progress-bar-point'+active+'"></div>';
		}														

		txt+='			</div>';
		if (data["strength_tip"] && data["strength_tip"] !== "") {
			txt += '<div class="ftt-profile__info-label"><a href="/edit/">'+data["strength_tip"]+'</a></div>';
		}										
		txt+='		</div>\
				</div>';

	return txt;
}

function init_thanks(lastid, o) {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden').remove();
	
	if (o != undefined) {
		$(o).addClass('loadb');				
	}
	
	var add = '';
	if (lastid){
		add = '&lastid='+lastid;
	}

	$.getJSON("/api/v2/thankslist?uid="+INTERFACE["uid"]+add+salt(),
		function(data){
			if (data.status && data.status == 200) {
				
				
				if (o != undefined) {
					var place = $(o).parent().parent();
					var txt = draw_thankslog_list(data.res);
					if (data.res.length == parseInt(data.portion)){
						txt+='<div class="ftt-profile__info"><div class="ftt-maintabs__load-more ftt-chat-aside__load-button">\
								<a href="" class="ftt-button" onclick="init_thanks('+data.lastid+', this);return false;">'+FLang["Showmore"]+'</a>\
							</div></div>';
					$(txt).insertBefore(place);
					$(place).remove();
					$("#container").removeClass('loadb');
					}
				} else {
					var txt = '<div class="ftt-page ftt-page-profile">\
						<div class="ftt-page__wrap">\
							<div class="ftt-profile">\
								<div class="ftt-profile__tips">';
			
							txt += draw_thankslog_list(data.res);
							if (data.res.length == parseInt(data.portion)){
								txt+='<div class="ftt-profile__info"><div class="ftt-maintabs__load-more ftt-chat-aside__load-button">\
										<a href="" class="ftt-button" onclick="init_thanks('+data.lastid+', this);return false;">'+FLang["Showmore"]+'</a>\
									</div></div>';
							}
					txt+='		</div>\
							</div>\
						</div>\
					</div>';
					$("#container").html(txt).removeClass('loadb');
				}

				
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function draw_thankslog_list(data) {

	r = new RegExp("\n","ig");
	txt = '';
	for ( i = 0; i < data.length; i++ ){
		var string = "";
		if (parseInt(data[i]["tip"]) === 1) {
			string = " "+FLang["for_tip"];
		}
		txt+= '<div class="ftt-tile ftt-tile_tip">\
			<div class="ftt-tile__container">\
				<a href="/profile/id'+data[i]["uid"]+'/"><div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data[i]["pic"]+'\')"></div></a>\
				<div class="ftt-tile__name"><a href="/profile/id'+data[i]["uid"]+'/">'+data[i]["name"]+'</a> '+FLang["thanked_"+data[i]["sex"]]+' <A href="/profile/id'+data[i]["desc"]["uid"]+'/">'+data[i]["desc"]["name"]+'</a> '+string+'</a></div>\
			</div>';
		if (parseInt(data[i]["tip"]) === 1) {
			txt += '<div class="ftt-tile_answer">'+draw_tip(data[i]["desc"], r, 1, 0)+'</div>'		
		}
		txt+='</div>';
	}
return txt;
}

function init_profile() {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden').remove();
	var add = '';
	cid = INTERFACE["gid"];
	if (cid > 0){ add = "&cid="+cid }
	$.getJSON("/api/v2/profile?uid="+INTERFACE["uid"]+add+salt(),
		function(data){
			if (data.status && data.status == 200) {
				var age = '';
				if (parseInt(data.profile["age"]) > 0) { age = ', '+data.profile["age"];}
				var home = '';
				if (data.profile["city_home"] !== ""){ home = '<div class="ftt-profile__city">'+FLang["Lives_in"]+' '+data.profile["city_home"]+'</div>'; }
				slink = '';
				if (data.profile["slink"] !== undefined && data.profile["slink"] !== ""){
					slink = '<div class="ftt-profile__social">\
								<a href="'+data.profile["slink"]+'"><img src="/img/icons/'+data.profile["soc"]+'.svg" alt=""></a>\
							</div>';
				}
				
				txt = '<div class="ftt-page ftt-page-profile">\
							<div class="ftt-page__wrap">\
								<div class="ftt-profile js-profile">\
									<div class="ftt-profile__images js-profileImagesSwiper">\
										<div class="swiper-container">\
											<div class="swiper-wrapper">';
											
											if (data.profile["photos"].length) {
												for ( i = 0; i < data.profile["photos"].length; i++ ){
													txt += img_html(data.profile["photos"][i][0]);
												}
												if (data.profile["photos"].length < 3) {
													for ( j = 0; j < 3 - data.profile["photos"].length; j++ ){
														txt+=img_html('/img/no_icon.png');
													}													
												}
											} else {
												txt += img_html(data.profile["pic"]);
												for ( j = 0; j < 2; j++ ){
													txt+=img_html('/img/no_icon.png');
												}
											}
		
									txt+='	</div>\
										</div>\
										<div class="swiper-pagination"></div>\
									</div>';
								if (data.owner === 1){
									txt +='<div class="ftt-profile__edit">\
												<a href="/edit/" class="ftt-button style_border_orange">'+FLang["Edit_profile"]+'</a>\
										</div>';	
									//pin at avatar	
									if ($("span.ftt-minheader__user-avatar-numbers").length) {
										$("span.ftt-minheader__user-avatar-numbers").remove();
									}
								}
							var onl = '';
							if (parseInt(data.profile["was_ago"]["pin"]) === 1) {
								onl = '<div class="ftt-profile__status-icon"></div>';
							}
							txt+='	<div class="ftt-profile__head">\
										<div class="ftt-profile__title">'+data.profile["name"]+age+'</div>\
										'+home+'\
										<div class="ftt-profile__status">'+onl+data.profile["was_ago"]["text"]+'</div>\
										'+slink+'\
									</div>';
									
									if (data.profile["strength"]) {
										txt += draw_progress(data.profile);
									}
									
								txt+='<div class="ftt-profile__char">\
										<div class="ftt-profile__char-el">\
											<div class="ftt-profile__char-title">'+data.profile["stats"]["days"]+'</div>\
											<div class="ftt-profile__char-label">'+string_days(data.profile["stats"]["days"])+'</div>\
										</div>\
										<div class="ftt-profile__char-el">\
											<div class="ftt-profile__char-title">'+data.profile["stats"]["tips"]+'</div>\
											<div class="ftt-profile__char-label">'+string_tips(data.profile["stats"]["tips"])+'</div>\
										</div>\
										<div class="ftt-profile__char-el">\
											<div class="ftt-profile__char-title">'+data.profile["stats"]["cons"]+'</div>\
											<div class="ftt-profile__char-label">'+string_cons(data.profile["stats"]["cons"], data.profile["sex"])+'</div>\
										</div>\
										<div class="ftt-profile__char-el">\
											<div class="ftt-profile__char-title"><a href="/thanks/id'+data.profile["uid"]+'/">'+data.profile["stats"]["thanks"]+'</a></div>\
											<div class="ftt-profile__char-label"><a href="/thanks/id'+data.profile["uid"]+'/">'+FLang["Said_thanks"]+'</a></div>\
										</div>\
									</div>';
									if (data.visitors.length) {
										txt+='	<div class="ftt-profile__view">\
													<div class="ftt-profile__info-label">'+FLang["Profile_guests"]+'</div>\
													<div class="ftt-profile__view-slider js-profileViewSwiper">\
														<div class="swiper-container">\
															<div class="swiper-wrapper">\
																';

										for ( i=0; i < data.visitors.length; i++ ){
											age = '';
											online = '';
											if (data.visitors[i]["age"] > 0){ age = ', <span>'+data.visitors[i]["age"]+'</span>'}
											if (parseInt(data.visitors[i]["online"]) > 0) { online = '<div class="ftt-tile__status-online"></div>'}
											txt +=	'<div class="swiper-slide"><div class="ftt-users__item">\
															<a href="/profile/id'+data.visitors[i]["uid"]+'/"><div class="ftt-users__ava" style="background-image:url(\''+data.visitors[i]["pic"]+'\')">'+online+'</div></a>\
															<div class="ftt-users__title">'+data.visitors[i]["name"]+age+'</div>\
													</div></div>';
										}																

										txt+='				</div>\
														</div>\
													</div>\
												</div>';
									
									}
									
									
							txt+='	<div class="ftt-profile__info">';
										
									if (data.profile["about"] !== "") {
										var r = new RegExp("\n","ig");
										txt += '<div class="ftt-profile__info-el">\
													<div class="ftt-profile__info-label">'+FLang["About"]+'</div>\
												<p>'+data.profile["about"].replace(r, '<br>')+'</p>\
												</div>';
									}	

								txt+='	<div class="ftt-profile__info-el">\
											<div class="ftt-profile__info-label">'+FLang["Languages"]+'</div>\
											<p>'+profile_languages(data.profile.langs)+'</p>\
										</div>';
								if (data.profile["tripnext"]["city"] !== undefined && data.profile["tripnext"]["city"].length) {
									var r1 = new RegExp("[ -]","ig");
									var r2 = new RegExp("[,]","ig");
									url = make_url(data.profile["tripnext"]["co"], r1, r2);
									if (data.owner === 1){
										url += data.profile["tripnext"]["markid"]+'/';
									}
									
									txt += '<div class="ftt-profile__info-el">\
												<div class="ftt-profile__info-label">'+FLang["Upcoming_trip"]+'</div>\
												<div class="ftt-profile__info-tags">\
													<a href="'+url+'" style="text-decoration:none;"><div class="ftt-profile__info-tag">'+data.profile["tripnext"]["city"]+' '+data.profile["tripnext"]["date"]+'</div></a>\
												</div>\
											</div>';
								}		
										if (data.profile["trips"].length) {
											counter = '';
											if (data.profile["trips"].length > 2) { counter = ': '+data.profile["trips"].length }
											txt+='<div class="ftt-profile__info-el">\
													<div class="ftt-profile__info-label">'+FLang["Past_trips"]+counter+'</div>\
													<div class="ftt-profile__info-slider js-profileInfoTagSwiper">\
													<div class="swiper-container">\
															<div class="swiper-wrapper">';	
													
												for ( i = 0; i < data.profile["trips"].length; i++ ){
													txt += '<div class="swiper-slide"><div class="ftt-profile__info-tag">'+data.profile["trips"][i]+'</div></div>';
												}
										
											txt+='</div></div></div>\
												</div>';
										}
										
										if (data.profile["instagram"]["media"].length) {
											txt+='	<div class="ftt-profile__info-el">\
													<div class="ftt-profile__info-label"><a href="https://instagram.com/'+data.profile["instagram"]["login"]+'/">'+FLang["Instagram_photos"]+'</a></div>\
														<div class="ftt-profile__info-images js-profileInfoImagesSwiper">\
															<div class="swiper-container">\
																<div class="swiper-wrapper">';	
												for ( i = 0; i < data.profile["instagram"]["media"].length; i++ ){
													txt += img_insta_html(data.profile["instagram"]["media"][i], data.profile["instagram"]["login"]);
												}				
																
											txt+='				</div>\
															</div>\
														</div>\
													</div>';
										}
										
										if (data.profile["countries"].length) {
											counter = '';
											if (data.profile["countries"].length > 2) { counter = ': '+data.profile["countries"].length }
											txt+='<div class="ftt-profile__info-el">\
													<div class="ftt-profile__info-label">'+FLang["Visited_cons"]+counter+'</div>\
													<div class="ftt-profile__info-slider js-profileInfoTagSwiper">\
														<div class="swiper-container">\
															<div class="swiper-wrapper">';	
													
												for ( i = 0; i < data.profile["countries"].length; i++ ){
													txt += '<div class="swiper-slide"><div class="ftt-profile__info-tag">'+data.profile["countries"][i]+'</div></div>';
												}
										
											txt+='</div></div></div>\
												</div>';
										}
										if (data.profile["worldmap"].length > 1) {
											txt+='<div class="ftt-profile__info-el">\
													<div class="ftt-profile__info-images"><img src="'+data.profile["worldmap"]+'" width="100%"></div>\
												</div>';
										}
							txt+='	</div>';
									if (data.profile["content"] !== undefined && data.profile["content"]["last"] !== undefined && data.profile["content"]["last"].length) {
										txt += '<div class="ftt-profile__tips">' + draw_list_tips(data.profile["content"]["last"])+'</div>';
									}
									
									if (data.profile["thankslog"] !== undefined && data.profile["thankslog"].length) {
										txt += '<div class="ftt-profile__tips">' + draw_thankslog_list(data.profile["thankslog"])+'</div>';
									}
									
							if (data.owner !== 1){
								var add = '';
								var cid = INTERFACE["gid"];
								if (cid > 0){ add = cid+'/' }
								txt+=	'<div class="ftt-profile__button js-profileStickyButton">\
											<a href="/inbox/'+data.profile["uid"]+'/'+add+'" class="ftt-button style_orange">'+FLang["Message"]+'</a>\
										</div>';
							}				

						txt+='	</div>\
							</div>\
						</div>';

				$("#container").html(txt).removeClass('loadb');
				var profileImagesSwiper = new Swiper('.js-profileImagesSwiper .swiper-container', {
					speed: 400,
					slidesPerView: 'auto',
					spaceBetween: 1,
					pagination: {
						el: '.swiper-pagination',
						clickable: true
					}
				});

				var profileInfoImagesSwiper = new Swiper('.js-profileInfoImagesSwiper .swiper-container', {
					speed: 400,
					slidesPerView: 'auto',
					spaceBetween: 1
				});

				var profileInfoTagSwiper = new Swiper('.js-profileInfoTagSwiper .swiper-container', {
					speed: 400,
					slidesPerView: 'auto',
					spaceBetween: 8
				});

				var profileViewSwiper = new Swiper('.js-profileViewSwiper .swiper-container', {
					speed: 400,
					slidesPerView: 'auto',
					spaceBetween: 8
				});
				
				if (data.owner !== 1){
					$(window).on('scroll', function(){
						profileStickyButtonScroll();
					});

					profileStickyButtonScroll();				
				}
				
				$('[data-fancybox]').fancybox({
					buttons: ['close']
				});
				
			} else {
				exeption(data, o);
			}
		}
	);
}


function chatScrollToBottom(){
	if($('.ftt-chat-message__scroll').length > 0) $('.ftt-chat-message__scroll').scrollTop($('.ftt-chat-message__scroll-container').height());
}


function init_settings() {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden').remove();
	$("#menu_settings").addClass('is-active');
	$.getJSON("/api/v2/loadsettings?"+salt(),
		function(data){
			if (data.status && data.status == 200) {
				txt = '<div class="ftt-page ftt-page-settings">\
							<div class="ftt-page__wrap">\
								<form action="" method="post" class="ftt-settings-form">\
									<div class="ftt-settings-form__group">\
									</div>\
									<div class="ftt-settings-form__group">\
										<div class="ftt-settings__label">'+FLang["Settings"]+'</div>\
									</div>\
									<!--div class="ftt-settings__label">Instagram</div-->\
									<!--div class="ftt-settings-form__group">\
										<div class="ftt-settings-form__control">\
											<a href="#" class="ftt-button style_light_orange">Add instagram</a>\
										</div>\
									</div-->\
									<!--div class="ftt-settings-form__group">\
										<div class="ftt-settings-form__control">\
											<a href="#" class="ftt-button style_orange">Send feedback</a>\
										</div>\
									</div-->\
									<div class="ftt-settings-form__group">\
										<!--div class="ftt-settings-form__control ftt-settings-form__control--flex">\
											<div class="ftt-settings__label">'+FLang["Hidden_mode"]+'</div>\
											<label class="ftt-checkbox-slider">\
												<input type="checkbox" id="s_hidden">\
												<span class="ftt-checkbox-slider__icon"></span>\
											</label>\
										</div-->\
									</div>\
			                        <!--div class="ftt-settings-form__control ftt-settings-form__control--flex">\
										<div class="ftt-settings__label">Advertising</div>\
										<label class="ftt-checkbox-slider">\
											<input type="checkbox" id="s_advert">\
											<span class="ftt-checkbox-slider__icon"></span>\
										</label>\
									</div-->\
									<div class="ftt-settings-form__group">\
										<div class="ftt-settings-form__control">\
											<a href="" class="ftt-button style_light_orange" onclick="logout(this);return false;">'+FLang["Logout"]+'</a>\
										</div>\
									</div>\
								</form>\
							</div>\
						</div>';

				$("#container").html(txt).removeClass('loadb');
				if ( parseInt(data.advert) === 1 ){
					$("#s_advert").attr('checked', 'checked');
				}
				if ( parseInt(data.hidden) === 1 ){
					$("#s_hidden").attr('checked', 'checked');
				}
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function init_chat() {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden');
	$("#menu_chat").addClass('is-active');
	$.getJSON("/api/v2/dialoglist?"+salt(),
		function(data){
			if (data.status && data.status == 200) {
				txt = ' <div class="ftt-page ftt-chat-page">\
						<div class="ftt-chat">\
							<div class="ftt-chat-section js-chatMessageSection">\
								<div class="ftt-chat-message js-chatMessageContainer">\
									<div class="ftt-chat__head">\
										<a href="" class="ftt-chat__return" onclick="hide_chat();return false;"></a>\
										<div class="ftt-chat__title">\
											<a href="/profile/id'+cuid()+'" id="opponent_link" style="text-decoration:none;color:white;"><div class="ftt-chat__head-avatar" id="opponent_ava"></div><span id="opponent_name"></span></div></a>\
										</div>\
										<input type="hidden" id="opponent" value="0">\
										<input type="hidden" id="last_id" value="0">\
										<div class="ftt-chat-message__scroll">\
											<div class="ftt-chat-message__scroll-container" id="thread">';
					txt+='	</div></div>\
							<div class="ftt-chat-message-send">\
								<form action="#" class="ftt-chat-message-form">\
									<textarea class="ftt-chat-message-input" placeholder="'+FLang["Text_message"]+'"></textarea>\
									<button class="ftt-chat-message-submit" onclick="post_message(this);"></button>\
								</form>\
							</div>\
						';
                    
				txt+='			</div>\
						</div>\
					<div class="ftt-chat-aside">\
						<div class="ftt-chat-interlocutor">\
							<div class="ftt-chat__head">\
								<a href="/inbox/" class="ftt-chat__return"></a>\
								<div class="ftt-chat__title">'+FLang["Chats"]+'</div>\
							</div>\
							<div class="ftt-interlocutor-list">'+draw_dialog_list(data.dialogs)+'</div>';
				if (data.dialogs.length == parseInt(data.portion)){
					txt+='<div class="ftt-maintabs__load-more ftt-chat-aside__load-button">\
                            <a href="" class="ftt-button" onclick="more_dialogs('+data.dialogs[data.dialogs.length-1]["idm"]+', this);return false;">'+FLang["Showmore"]+'</a>\
                        </div>';
				}
							
				txt+='	</div>\
					</div>';
				 txt+='</div>\
				</div>';

				$("#container").html(txt).removeClass('loadb');
				url_opon();
				$(window).width() >= 640 && chatMessageContainerHeight();
								
			} else {
				exeption(data, o);
			}
		}
	);
}

function more_dialogs(lastid, o){
	if (! lastid) {return;}
	$(o).addClass('loadb');
	$.getJSON("/api/v2/dialoglist?lastid="+lastid+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				$("div.ftt-interlocutor-list").append(draw_dialog_list(data.dialogs));
				if (data.dialogs.length == parseInt(data.portion)){
					$('<a href="" class="ftt-button" onclick="more_dialogs('+data.dialogs[data.dialogs.length-1]["idm"]+', this);return false;">'+FLang["Showmore"]+'</a>').insertAfter(o);
					$(o).remove();
				} else {
					$(o).parent().remove();
				}
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function chatMessageContainerHeight(){
	var screenHeight = $(window).height() - $('.ftt-minheader').height();

	$('.js-chatMessageContainer').css({
		'height': screenHeight
	});
}

function url_opon(){
	if (CHAT_USER === null || CHAT_USER === undefined) {
		return '';
	}
	if (! $("#di_"+CHAT_USER["uid"]).length) {
	  $(".ftt-interlocutor-list").prepend(draw_dialog_list([{
         "snip": "",
         "idm": 0,
         "uid": CHAT_USER["uid"],
         "name": CHAT_USER["name"],
         "sex": 0,
         "age": 25,
         "pic": CHAT_USER["ava"],
         "cnt": 0,
         "city": ""
      }]));
	}
	$("#di_"+CHAT_USER["uid"]).trigger('click');
}

function show_chat(uid, o) {	
	$("#thread", o).addClass('loadb');
		$.getJSON("/api/v2/loadthread_new?uid="+uid+salt(),
		function(data){
			if (data.status && data.status == 200) {

				txt, last_id = draw_messages(data);
						
				$("#thread").html(txt).removeClass('loadb');
				scrollPosition = $(window).scrollTop();
				$('body').addClass('is-chat-open');
				$("#opponent").val(uid);
				$("#last_id").val(last_id);
				$('div.ftt-interlocutor__elem').removeClass('is-selected');
				$(o).removeClass('loadb').addClass('is-selected');
				$("#opponent_name").html($(o).find("div.ftt-interlocutor__name").html());
				$("#opponent_link").attr("href", '/profile/id'+uid);
				$('#opponent_ava').css("background-image", $(o).find("div.ftt-interlocutor__avatar").css("background-image")); 
				$(o).find("div.ftt-interlocutor__number").remove();
				chatScrollToBottom();
				var draft = '';
				if (data.draft.length > 0) {
					draft = data.draft;
				}
				$(".ftt-chat-message-input").text(draft).focus();
			} else {
				exeption(data, o);
			}
		}
	);
}

function draw_messages(data){
	txt = '';					
	last_id = 0;
	time = 0;
	for ( i = 0; i < data.mes.length; i++ ){
		
		if ( $("#mes_"+data.mes[i]["id"]).length ) {
			continue;
		}

		date = '';
		ans = '';
		img = '';
		readed = '';
		last_id = data.mes[i]["id"];
		
		if (i > 0 && calculate_day(data.mes[i]["time"]) !== calculate_day(data.mes[i-1]["time"])){ 
			date = ' <div class="ftt-chat-message__date">'+dayFromTimestamp(data.mes[i]["time"])+'</div>';
		} 	
		
		if (data.id !== data.mes[i]["sender"]) { ans = ' message_answer'
		} else {
			if (data.mes[i]["readed"] == 0) { readed = '<div class="ftt-chat-message__elem-tick"></div>'}
			if (data.mes[i]["readed"] == 1) { readed = '<div class="ftt-chat-message__elem-tick"></div><div class="ftt-chat-message__elem-tick"></div>'}		
		}
		if (data.mes[i]["img"].length) {
			for ( j = 0; j < data.mes[i]["img"].length; j++ ){
				img += '<img src="'+data.mes[i]["img"][j]+'" alt="" class="ftt-chat-message__elem-image">'
			}
		}

		txt += date+'<div class="ftt-chat-message__elem'+ans+'" id="mes_'+data.mes[i]["id"]+'">\
					<div class="ftt-chat-message__elem-text">'+ quote(data.mes[i]["mes"])+img+readed+'</div>\
				</div>';
	}
	
	return txt, last_id;
	
}

function gotip(id, o){
	$(o).addClass('loadb');
	$.getJSON("/api/v2/urlbyid?id="+id+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				href_url(data.url);
			} else {
				exeption(data, o);
			}
		}
	);
}

function quote(mes) {
	var tip = mes.match(/::(\d+)::(\d+)::\n\[\[([^\]]+)\]\]\n([\S\s]+)$/);
	if (! tip || ! tip.length || tip.length < 5){
		return mes;
	}
	
	name = CURRENT_USER["name"];
	if (parseInt(tip[1]) !== cuid()){
		name = $("#opponent_name").html();
	}
	
	return '<div class="ftt-chat-message__elem-quote" onclick="gotip('+tip[2]+', this);return false;">\
				<b>'+name+'</b>\
				'+tip[3]+'\
			</div>'+tip[4];
}

function dayFromTimestamp(time){
	
	var d = new Date();
    var today = d.getDate();
	var y = d.getFullYear();
	
	var date = new Date(time * 1000);
	var day = date.getDate();
	var year = date.getFullYear();
	
	if (today === day && y === year) {
		return FLang["Today"];
	}
	
	if (today - 1 === day && y === year) {
		return FLang["Yesterday"];
	}
	
	return FLang['mon_'+date.getMonth()]+' '+date.getDate();
}

function calculate_day(time){
	return 86400 * parseInt(time / 86400)
}

function chat_refresh() {	
	uid = parseInt($("#opponent").val());
	lastid = parseInt($("#last_id").val());
	
	if (uid <= 0 || lastid <= 0) {
		return;
	}	
	
	$.getJSON("/api/v2/loadthread_new?uid="+uid+"&lastid="+lastid+salt(),
	function(data){
		if (data.status && data.status == 200 && data.mes.length > 0) {
				
			txt, last_id = draw_messages(data);
			$("#last_id").val(last_id);
			$("#thread").append(txt);
			chatScrollToBottom();
		}
	});	
	
}

function post_message(o){
	
	uid = parseInt($("#opponent").val());
	lastid = parseInt($("#last_id").val());
	mes = $(".ftt-chat-message-input").val();
	if (uid <= 0 || mes.length === 0 ) {
		return;
	}
	$(".ftt-chat-message-send").addClass('loadb');
	$(o).attr('disabled', true);
	
	add = '';
	cid = INTERFACE["gid"];
	if (cid > 0){ add = "&cid="+cid }
	
	$.post("/api/v2/message", "&uid="+uid+add+"&mes="+encodeURIComponent(mes)+"&lastid="+lastid+salt(),
		function(data){
			$(".ftt-chat-message-send").removeClass('loadb');
			$(o).attr('disabled', false);
			if (data.status && data.status == 200 && data.mes.length > 0) {
				txt, last_id = draw_messages(data);
				$("#last_id").val(last_id);
				$("#thread").append(txt);
				chatScrollToBottom();
				$(".ftt-chat-message-input").val('').focus();
			} else {
					
				if (data.strength) {
					
					$("#warningModal div.ftt-fieldset").html(draw_progress(data));
					$("#warningModal button").html(FLang["Edit_profile"]);
					$("#warningModal div.ftt-modal-tips-mobheader__title, #warningModal div.ftt-modal-tips-title").html(FLang["Add_more_info"]);
					$("#warningModal div.ftt-modal-tips-body p").html(FLang["Why_more_info"]);
					
					$.magnificPopup.open({
						items: {
							src: $("#warningModal"),
							type: 'inline'
						},
						closeBtnInside: true,
						callbacks: {
							open: function() {
								scrollPosition = $(window).scrollTop();
								$('body').addClass('mfp-open');
							},
							close: function(){
								$('body').removeClass('mfp-open');
								$(window).width() <= 767 && $('body, html').scrollTop(scrollPosition);
							}
						}
					});

					$(document).on('click', '.js-modalClose', function(){
						$.magnificPopup.close();
					});
				
				} else {
					exeption(data, o);
				}

			}
		}, "json"
	);
}


function hide_chat() {	
	$('body').removeClass('is-chat-open');
	$('body, html').scrollTop(scrollPosition);
}


function draw_dialog_list(data){
	txt = '';
	var curuid = cuid();
	
	for ( i=0; i < data.length; i++ ){
		city = '';
		cnt = '';
		you = '';
		if (data[i]["city"].length) { city = ', '+data[i]["city"]; }
		if (parseInt(data[i]["cnt"]) > 0) { cnt = '<div class="ftt-interlocutor__number">'+data[i]["cnt"]+'</div>'; }
		if (parseInt(data[i]["uid"]) === curuid){ you =' <span>'+FLang["You"]+':</span>' } 
		txt += '<div class="ftt-interlocutor__elem" onclick="show_chat('+data[i]["uid"]+', this);" id="di_'+data[i]["uid"]+'">\
					<div class="ftt-interlocutor__avatar" style="background-image: url(\'/img/ava/'+data[i]["pic"]+'\')"></div>\
					<div class="ftt-interlocutor__info">\
						<div class="ftt-interlocutor__name">'+data[i]["name"]+city+'</div>\
						<div class="ftt-interlocutor__message">'+you+data[i]["snip"]+'</div>\
						<a href="/profile/id'+data[i]["uid"]+'/" class="ftt-interlocutor__profile-link">'+FLang["Showprofile"]+'</a>\
						'+cnt+'\
					</div>\
				</div>';
	}
	return txt;
}

function redir_main(){
	document.location.href="/";
}

function redir_main_page(){
	var base = "/";
	if (INTERFACE !== null && INTERFACE !== undefined && INTERFACE["tab"] === "feed"){
		base = "/feed/"
	}
	document.location.href=base;
}

function init_feed_new(cid) {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden');
	$("#menu_feed").addClass('is-active');
	add = '';
	cid = INTERFACE["cid"];
	if (cid > 0){ add = "&cid="+cid }

	$.getJSON("/go/feed?uid=" + cuid()+salt(),
		function(data){
			if (data.status && data.status == 200) {
				txt = '\
						<div class="ftt-maintabs ftt-maintabs_mini">\
							<div class="ftt-maintabs__item ftt-maintabs__tips">\
								<div class="ftt-tabhead ftt-tabhead_mini">\
									<a href="/'+(INTERFACE["url"] != "" ? INTERFACE["url"]+'/' : '' )+'" class="ftt-tabhead__btn">'+FLang["People"]+'</a>\
									<span class="ftt-tabhead__btn ftt-tabhead__btn_active">'+FLang["Feed"]+'</span>\
								</div>\
								<div class="ftt-tabhead__add">\
									<div class="ftt-add-dropdown" id="add_tip">\
										<div class="ftt-add-dropdown__btn" id="add_tip_b">'+FLang["Add"]+'</div>\
										<div class="ftt-add-dropdown__list">\
											<div class="ftt-add-dropdown__elem js-modalLink" id="t_tip" data-mfp-src="#tipsModal">'+FLang["Tip"]+'</div>\
											<div class="ftt-add-dropdown__elem js-modalLink" id="t_qst" data-mfp-src="#tipsModal">'+FLang["Question"]+'</div>\
										</div>\
									</div>\
								</div>';
								
				if ( data.leaders !== undefined && data.leaders.length > 0 ){

						txt+='<h2 class="ftt-h2">'+FLang["Leaders"]+'</h2>\
									<div class="ftt-users">\
										<div class="ftt-users__container">';
									
								for ( i=0; i < data.leaders.length; i++ ){
									age = '';
									online = '';
									if (data.leaders[i]["age"] > 0){ age = ', <span>'+data.leaders[i]["age"]+'</span>'}
									if (parseInt(data.leaders[i]["online"]) > 0) { online = '<div class="ftt-tile__status-online"></div>'}
									txt +=	'<div class="ftt-users__item">\
													<a href="/profile/id'+data.leaders[i]["uid"]+'/'+data.cid+'/"><div class="ftt-users__ava" style="background-image:url(\''+data.leaders[i]["pic"]+'\')">'+online+'</div></a>\
													<div class="ftt-users__title">'+data.leaders[i]["name"]+age+'</div>\
											</div>';
								}
		
							txt +='	</div>\
								</div>';				
				}

				txt +='<h2 class="ftt-h2">'+FLang["Recent_feed"]+'</h2>\
								<div class="ftt-tiles ftt-tiles_tips" id="feed">\
									'+draw_list_tips(data.last)+'\
								</div>';
							
							if (data.portion <= data.last.length && parseInt(data.lastid) != 10003) {
								txt+='	<div class="ftt-maintabs__load-more" id="morebut">\
										<a href="#" class="ftt-button" onclick="refresh_list_feed('+data.lastid+');return false;">'+FLang["Showmore"]+'</a>\
									</div>';	
							}		

								
					txt+='	</div>\
						</div>';
						
				$("#container").html(txt).removeClass('loadb');
				$("#feed div.ftt-tile__text").each(function(i, obj) {
					if ($(obj)[0].scrollHeight >  $(obj).innerHeight()) {
						read_more(obj);
					}
				});
				
				$(document).on('click', '#add_tip_b', function(event){
					event.stopPropagation();
					$(this).parent().toggleClass('is-open');
					return false;
				});

				$(document).on('click', function(){
					$('#add_tip').removeClass('is-open');
				});
				
			
				$(document).on('click', '.js-modalLink', function(event) {
					modal_captions(this);
					event.preventDefault();

					var src = $(this).data('mfp-src'),
						type = $(this).data('mfp-ajax') || 'inline';

					$.magnificPopup.open({
						items: {
							src: src,
							type: type
						},
						closeBtnInside: true,
						callbacks: {
							open: function() {
								scrollPosition = $(window).scrollTop();
								$('body').addClass('mfp-open');
								setTimeout(function() { $("#add_mes").focus(); }, 500);
							},
							close: function(){
								$('body').removeClass('mfp-open');
								$(window).width() <= 767 && $('body, html').scrollTop(scrollPosition);
							}
						}
					});
				});

				$(document).on('click', '.js-modalClose', function(){
					$.magnificPopup.close();
				});
				
				if (data.last.length){
					qstvisit(data.last[0]["qid"]);
				}
				
			
			} else {
				exeption(data, o);
			}
		}
	);
}

function init_feed(cid) {
	$("#container").addClass('loadb');
	$("#filter_burger").addClass('hidden');
	$("#menu_feed").addClass('is-active');
	add = '';
	cid = INTERFACE["cid"];
	if (cid > 0){ add = "&cid="+cid }
	$.getJSON("/api/v2/qstlist_new?"+add+salt(),
		function(data){
			if (data.status && data.status == 200) {
				txt = '\
						<div class="ftt-maintabs ftt-maintabs_mini">\
							<div class="ftt-maintabs__item ftt-maintabs__tips">\
								<div class="ftt-tabhead ftt-tabhead_mini">\
									<a href="/'+(INTERFACE["url"] != "" ? INTERFACE["url"]+'/' : '' )+'" class="ftt-tabhead__btn">'+FLang["People"]+'</a>\
									<span class="ftt-tabhead__btn ftt-tabhead__btn_active">'+FLang["Feed"]+'</span>\
								</div>\
								<div class="ftt-tabhead__add">\
									<div class="ftt-add-dropdown" id="add_tip">\
										<div class="ftt-add-dropdown__btn" id="add_tip_b">'+FLang["Add"]+'</div>\
										<div class="ftt-add-dropdown__list">\
											<div class="ftt-add-dropdown__elem js-modalLink" id="t_tip" data-mfp-src="#tipsModal">'+FLang["Tip"]+'</div>\
											<div class="ftt-add-dropdown__elem js-modalLink" id="t_qst" data-mfp-src="#tipsModal">'+FLang["Question"]+'</div>\
										</div>\
									</div>\
								</div>';
								
				if ( data.leaders !== undefined && data.leaders.length > 0 ){

						txt+='<h2 class="ftt-h2">'+FLang["Leaders"]+'</h2>\
									<div class="ftt-users">\
										<div class="ftt-users__container">';
									
								for ( i=0; i < data.leaders.length; i++ ){
									age = '';
									online = '';
									if (data.leaders[i]["age"] > 0){ age = ', <span>'+data.leaders[i]["age"]+'</span>'}
									if (parseInt(data.leaders[i]["online"]) > 0) { online = '<div class="ftt-tile__status-online"></div>'}
									txt +=	'<div class="ftt-users__item">\
													<a href="/profile/id'+data.leaders[i]["uid"]+'/'+data.cid+'/"><div class="ftt-users__ava" style="background-image:url(\''+data.leaders[i]["pic"]+'\')">'+online+'</div></a>\
													<div class="ftt-users__title">'+data.leaders[i]["name"]+age+'</div>\
											</div>';
								}
		
							txt +='	</div>\
								</div>';				
				}

				txt +='<h2 class="ftt-h2">'+FLang["Recent_feed"]+'</h2>\
								<div class="ftt-tiles ftt-tiles_tips" id="feed">\
									'+draw_list_tips(data.last)+'\
								</div>';
							
							if (data.portion <= data.last.length) {
								txt+='	<div class="ftt-maintabs__load-more" id="morebut">\
										<a href="#" class="ftt-button" onclick="refresh_list_feed('+data.lastid+');return false;">'+FLang["Showmore"]+'</a>\
									</div>';	
							}		

								
					txt+='	</div>\
						</div>';
						
				$("#container").html(txt).removeClass('loadb');
				$("#feed div.ftt-tile__text").each(function(i, obj) {
					if ($(obj)[0].scrollHeight >  $(obj).innerHeight()) {
						read_more(obj);
					}
				});
				
				$(document).on('click', '#add_tip_b', function(event){
					event.stopPropagation();
					$(this).parent().toggleClass('is-open');
					return false;
				});

				$(document).on('click', function(){
					$('#add_tip').removeClass('is-open');
				});
				
			
				$(document).on('click', '.js-modalLink', function(event) {
					modal_captions(this);
					event.preventDefault();

					var src = $(this).data('mfp-src'),
						type = $(this).data('mfp-ajax') || 'inline';

					$.magnificPopup.open({
						items: {
							src: src,
							type: type
						},
						closeBtnInside: true,
						callbacks: {
							open: function() {
								scrollPosition = $(window).scrollTop();
								$('body').addClass('mfp-open');
								setTimeout(function() { $("#add_mes").focus(); }, 500);
							},
							close: function(){
								$('body').removeClass('mfp-open');
								$(window).width() <= 767 && $('body, html').scrollTop(scrollPosition);
							}
						}
					});
				});

				$(document).on('click', '.js-modalClose', function(){
					$.magnificPopup.close();
				});
				
				if (data.last.length){
					qstvisit(data.last[0]["qid"]);
				}
				
			
			} else {
				exeption(data, o);
			}
		}
	);
}

function add_mark(o){
	
	from = parseInt($("#mark_from").val());
	to = parseInt($("#mark_to").val());
	if ( from.length === 0 || to.length === 0 ) {
		return;
	}

	$(o).attr('disabled', true).addClass('loadb');

	$.post("/api/v2/flip", "&"+$("form.ftt-modal-tips-form").serialize()+salt(),
		function(data){

			$(o).attr('disabled', false).removeClass('loadb');
			if (data.status && data.status == 200) {
				window.location.replace(window.location.pathname + window.location.search + window.location.hash);
			} else {
				exeption(data, o);
			}
		}, "json"
	);
}

function add_tip(o){
	
	mes = $("#add_mes").val();
	if ( mes.length === 0 ) {
		$("#add_mes").focus();
		return;
	}
	$("#add_mes").addClass('loadb');
	$(o).attr('disabled', true).addClass('loadb');

	$.post("/api/v2/postqst", "&"+$("form.ftt-modal-tips-form").serialize()+salt(),
		function(data){
			$("#add_mes").removeClass('loadb');
			$(o).attr('disabled', false).removeClass('loadb');
			if (data.status && data.status == 200) {
				window.location.replace(window.location.pathname + window.location.search + window.location.hash);
			} else {
				exeption(data, o);
				$("#add_mes").focus();
			}
		}, "json"
	);
}


function modal_captions(o){
	$("#tipsModal button.ftt-button").html(FLang["Send"]);
	
	$("#tipsModal div.ftt-modal-tips-mobheader__title, #tipsModal div.ftt-modal-tips-title").html(FLang["My_tip"]);
	$("#tipsModal textarea.ftt-input").attr("placeholder", FLang["Tip_placeholder"]);
	$("#intro").html(FLang["Tip_intro"]);
	$("#tip_type").val(1);
	
	if ($(o).attr('id') === "t_qst") {
		$("#tipsModal div.ftt-modal-tips-mobheader__title, #tipsModal div.ftt-modal-tips-title").html(FLang["My_qst"]);
		$("#tipsModal textarea.ftt-input").attr("placeholder", FLang["Qst_placeholder"]);
		$("#intro").html(FLang["Qst_intro"]);
		$("#tip_type").val(0);
	}
}

function refresh_list_feed(lastid){
	add = '';
	cid = INTERFACE["cid"];
	if (cid > 0){ add = "&cid="+cid }
	if (lastid !== undefined && lastid > 0){ add += '&lastid='+lastid; }
	$("#container").addClass('loadb');
	
	url = "/api/v2/qstlist_new?"+add+salt();
	if (cuid() == 5082896) {
		url = "/go/feed?uid=" + cuid() + add
	}
	
	$.getJSON(url,
		function(data){
			if (data.status && data.status == 200) {
				if (data.last && data.last.length) {
					$("#feed").append(draw_list_tips(data.last));
				}
				if (data.last && data.portion <= data.last.length &&  parseInt(data.lastid) != 10003) {
					$("#morebut").html('<a href="#" class="ftt-button" onclick="refresh_list_feed('+data.lastid+');return false;">'+FLang["Showmore"]+'</a>')
				} else {
					$("#morebut").remove();
				}
				$("#container").removeClass('loadb');
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function read_more(obj){
	r = new RegExp("<br>","ig");
	r1 = new RegExp("\n","ig");
	txt = $(obj).html().replace(r, "\n");
	$(obj).html(txt);
    var innerText = $(obj).text();
    var textArray = innerText.split('');
    var formatted = '';
    for(var i in textArray) {
        formatted += '<span>' + textArray[i] + '</span>';
    }
    var heightOfContainer = $(obj).height();
    $(obj).html(formatted);
    var clipped = '';
    $(obj).find('span').each(function(){
        if ($(this).position().top < heightOfContainer) {
            clipped += $(this).text();
        } else {
            return false;
        }
    });
	pure = clipped;
	clipped = trim_br(clipped.replace(r1, "<br>").trim());
    clipped += '... <a href="'+get_link(obj)+'">'+FLang["Read_more"]+'</a>';
    $(obj).html(clipped);
	get_link(obj);
	
	while (check_visible(obj)){
		pure = pure.slice(0,-15);
		$(obj).html(trim_br(pure.replace(r1, "<br>").trim())+'... <a href="'+get_link(obj)+'">'+FLang["Read_more"]+'</a>');
	}
}

function get_link(obj) {
	return $(obj).parent().find('div.ftt-tile__share').parent().attr("href");
}

function trim_br(clipped){
	if (clipped.substr(clipped.length - 4) === "<br>" || clipped.substr(clipped.length - 4) === "<BR>"){
		clipped = clipped.slice(0,-4);
	};
	return clipped;
}

function check_visible(element){
	var invisibleItems = [];
	for(var i=0; i<element.childElementCount; i++){
		if (element.children[i].tagName !== "A") { continue; }
		
	  if (element.children[i].offsetTop + element.children[i].offsetHeight >
		  element.offsetTop + element.offsetHeight ||
		  element.children[i].offsetLeft + element.children[i].offsetWidth >
		  element.offsetLeft + element.offsetWidth ){

			invisibleItems.push(element.children[i].tagName);
		}

	}
	if (invisibleItems.length) { return true }
	return false;
}

function lang_filters(data){
	txt = '	<div class="ftt-filter__group" id="group_la">\
				<div class="ftt-filter__label">'+FLang["Languages"]+'</div>';
				
		for ( i=0; i < data.length; i++ ){
			txt+='	<div class="ftt-filter__list-el" id="la_'+data[i]["id"]+'">\
						<div class="ftt-filter__list-del" onclick="del_lang(this);"></div>\
						<div class="ftt-filter__list-label">'+data[i]["val"]+'</div>\
					</div>'
		}
		if (data.length < 4) {
			txt+='<div class="ftt-filter__add-control" id="addmorela" onclick="lan_fil_select(this);">'+FLang["Add_more"]+'</div>';
		}
				
	txt+='</div>';
	return txt;
}

function lang_filters_profile(data){
	txt = '<div class="ftt-profile__info-el" id="group_la">\
				<div class="ftt-fieldset__label">'+FLang["Languages"]+'</div>\
					<div class="ftt-filter__list">';
				
		for ( i=0; i < data.length; i++ ){
			txt+='	<div class="ftt-filter__list-el" id="la_'+data[i]["id"]+'">\
						<div class="ftt-filter__list-del" onclick="del_lang(this, 1);"></div>\
						<div class="ftt-filter__list-label">'+data[i]["val"]+'</div>\
					</div>'
		}
		txt+='</div>';
		if (data.length < 4) {
			txt+='<div class="ftt-filter__add-control" id="addmorela" onclick="lan_fil_select(this, \'pro\');">'+FLang["Add_more"]+'</div>';
		}
				
	txt+='</div>';
	return txt;
}

function lan_fil_select(o, pro){
	if (! pro){ pro = ''}
	$(o).addClass('loadb');
	$.getJSON("/api/v2/languages?"+salt(),
		function(data){
			if (data.langs && data.langs.length) {
				txt = '<div class="ftt-filter__list-el"><select onchange="add_lang(this, \''+pro+'\');"><option value=""></option>';
				for ( i=0; i < data.langs.length; i++ ){
					txt+='<option value="'+data.langs[i]["id"]+'" name="">'+data.langs[i]["val"]+'</option>';
				}
				txt+='</select></div>';
				
				if (pro !== '') {
					$("#group_la div.ftt-filter__list").append(txt);
				} else {
					$(txt).insertBefore(o);
				}
				
				$(o).removeClass('loadb');
				if ($("#group_la div.ftt-filter__list-el" ).size() > 3) {
					$(o).remove();
					return;
				}
			} else {
				exeption(data, o);
			}
		}
	);
}

function add_lang(o, pro){
	method = 'fil';
	if (pro && pro !== ""){
		method = 'pro';
		$(o).addClass('loadb');
	} else {
		$("#filters").addClass('loadb');
		pro = '';
	}
	
	$.getJSON("/api/v2/add_"+method+"_lang?lid="+o.value+salt(),
		function(data){
			if (data.status && data.status == 200) {
				var parent = $(o).parent();
				$('<div class="ftt-filter__list-el" id="la_'+o.value+'">\
						<div class="ftt-filter__list-del" onclick="del_lang(this, \''+pro+'\');"></div>\
						<div class="ftt-filter__list-label">'+o.options[o.selectedIndex].text+'</div>\
					</div>').insertAfter(parent);
				$(parent).remove();
				
				if (method == "fil") {
					refresh_list_people();
					$("#filters").removeClass('loadb');				
				}

			} else {
				exeption(data, o);
			}
		}
	);
}

function del_lang(o, pro){
	var method = 'fil';
	if (pro && pro !== '') { 
		method = 'pro';
		$(o).addClass('loadb');
	} else {
		$("#filters").addClass('loadb');
		pro = '';
	}
	var id = $(o).parent().attr('id');
	var val = id.split('_')[1];
	
	$.getJSON("/api/v2/del_"+method+"_lang?lid="+val+salt(),
		function(data){
			
			$(o).removeClass('loadb');
		
			if (data.status && data.status == 200) {
				$("#"+id).remove();
				if (!$("#addmorela").length) {
					$("#group_la").append('<div class="ftt-filter__add-control" id="addmorela" onclick="lan_fil_select(this, \''+pro+'\');">'+FLang["Add_more"]+'</div>');
				}				
				if (method == "fil") {
					refresh_list_people();
					$("#filters").removeClass('loadb');				
				}
			} else {
				exeption(data, o);
			}
		}
	);
}

function init_search() {
	$("#container").addClass('loadb');
	$("#filter_burger").removeClass('hidden');
	add = '';
	cid = INTERFACE["cid"];
	if (cid > 0){ add = "&cid="+cid }
	
	gid = INTERFACE["gid"];
	if (gid > 0){ add += "&gid="+gid }
	
	$.getJSON("/api/v2/listpro?ppage=50"+add+salt(),
		function(data){
			if (data.status && data.status == 200) {
				txt = '<div class="ftt-main__grid">\
							<div class="ftt-main__section">\
								<div class="ftt-maintabs ftt-maintabs_mini">\
									<div class="ftt-maintabs__item">\
										<div class="ftt-tabhead ftt-tabhead_mini">\
											<span class="ftt-tabhead__btn ftt-tabhead__btn_active">'+FLang["People"]+'</span>\
											<a href="/feed/'+(INTERFACE["url"] != "" ? INTERFACE["url"]+'/' : '' )+'" class="ftt-tabhead__btn">'+FLang["Feed"]+'</a>\
										</div>';
				if (data.button == 1){
					txt += '<div class="ftt-maintabs__note-btn">\
								<div class="ftt-tabhead__btn ftt-tabhead__btn-note js-modalLink" data-mfp-src="#marksModal">'+FLang["Chekin"]+'</div>\
							</div>';
				}
								txt += '<div class="ftt-tiles ftt-tiles_list" id="people">';
				
				txt += draw_list_people(data);
				
				txt += '				</div>';
				
				/*
				//Button show more
				txt += '<div class="ftt-maintabs__load-more">\
                            <a href="#" class="ftt-button">'+FLang["Showmore"]+'</a>\
                        </div>';
				*/
				
				txt += '            </div>\
								</div>\
							</div>';
				
				txt += '<div class="ftt-main__aside">\
							<div class="ftt-main__filter">\
								<div class="ftt-filter" id="filters">\
									<div class="ftt-filter__mob-header">\
										<div class="ftt-mobmenu__return" onclick="back_filters_view();return false;"></div>\
									</div>\
									<div class="ftt-filter__scroll">';
				txt += ' 		   <div class="ftt-filter__group">\
										<label class="ftt-filter__radio">\
											<input type="radio" name="filter_radio" onclick="save_filter(\'sex\', 2);"'+( data["filters"]["female"] === 1 ? ' checked="checked"' : '' )+'>\
											<span>'+FLang["Showfemale"]+'</span>\
										</label>\
										<label class="ftt-filter__radio">\
											<input type="radio" name="filter_radio" onclick="save_filter(\'sex\', 1);"'+( data["filters"]["male"] === 1 ? ' checked="checked"' : '' )+'>\
											<span>'+FLang["Showmale"]+'</span>\
										</label>\
										<label class="ftt-filter__radio">\
											<input type="radio" name="filter_radio" onclick="save_filter(\'sex\', 0);"'+( data["filters"]["female"] === 0 && data["filters"]["male"] === 0 ? ' checked="checked"' : '' )+'>\
											<span>'+FLang["Showboth"]+'</span>\
										</label>\
									</div>\
									<div class="ftt-filter__group ftt-filter__group-range">\
										<div class="ftt-filter__label">'+FLang["Age"]+' <span id="f_from">'+data["filters"]["from"]+'</span> — <span id="f_to">'+data["filters"]["to"]+'</span></div>\
										<div class="ftt-filter__range">\
											<input type="text" class="js-rangeSlider" data-min="18" data-max="70"  data-from="'+data["filters"]["from"]+'" data-to="'+data["filters"]["to"]+'" value="">\
										</div>\
									</div>\
									'+lang_filters(data.filters.langs);
				txt += '        	</div>\
								</div>\
							</div>\
						</div>';
							
				txt += '</div>';
						
				$("#container").html(txt).removeClass('loadb');
				$('.js-rangeSlider').ionRangeSlider({
					type: 'double',
					min: $(this).data('min'),
					max: $(this).data('max'),
					from: $(this).data('from'),
					to: $(this).data('to'),
					force_edges: true,
					hide_from_to: true,
					hide_min_max: true,
					onChange: function (data) {
						$("#f_from").html(data.from);
						$("#f_to").html(data.to);
					}, onFinish: function (data) {
						save_filter('age', data.from, data.to);
					},
				}); 
				
				
				$(document).on('click', '.js-modalLink', function(event) {
					$("#marksModal button").html(FLang["Add_trip"]);
					$("#marksModal div.ftt-modal-tips-body p").html(FLang["My_trip_intro"]);
					$("#marksModal div.ftt-modal-tips-mobheader__title, #marksModal div.ftt-modal-tips-title").html(FLang["My_trip"]);
					$("#marksModal input.ftt-input").attr('placeholder', FLang["Select_dates"]);
					
					event.preventDefault();

					var src = $(this).data('mfp-src'),
						type = $(this).data('mfp-ajax') || 'inline';

					$.magnificPopup.open({
						items: {
							src: src,
							type: type
						},
						closeBtnInside: true,
						callbacks: {
							open: function() {
								scrollPosition = $(window).scrollTop();
								$('body').addClass('mfp-open');
							},
							close: function(){
								$('body').removeClass('mfp-open');
								$(window).width() <= 767 && $('body, html').scrollTop(scrollPosition);
							}
						}
					});
				});

				$(document).on('click', '.js-modalClose', function(){
					$.magnificPopup.close();
				});
				
				$('.js-datepickerRange').datepicker({
					range: true,
					multipleDatesSeparator: ' - ',
					autoClose: true,
					language: FLang["key"],
					onSelect: function(formattedDate, date) {
						if (date.length === 2){
							$("#mark_from").val((date[0]).toISOString().substring(0, 10));
							$("#mark_to").val((date[1]).toISOString().substring(0, 10));
						}
					}
				});
				
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function draw_tip(data, r, shortv, list){
	
	if (! r ){
		r = new RegExp("\n","ig");
	}
	city = data["city"];
	if ( data["city"] != data["country"] ) {
		city += ', '+data["country"];
	}
	type = '<div class="ftt-tile__label ftt-tile__label_question">'+FLang["Question"]+'</div>';
	if ( parseInt(data["tip"]) == 1 ) {
		type = '<div class="ftt-tile__label ftt-tile__label_advise">'+FLang["Tip"]+'</div>';
	}
	if (!list && cuid() === parseInt(data["uid"])) {
		var arg = data["country"];
		if (data["country"] !== data["city"]){
			arg = data["city"] + ', '+arg;
		}
		type = '\
		<div class="ftt-tile__setting">\
			<div class="ftt-trip-el__setting js-dropdownToggle">\
				<div class="ftt-trip-el__setting-control js-dropdownToggleControl">\
					<div class="ftt-trip-el__setting-dot"></div>\
				</div>\
				<div class="ftt-trip-el__setting-dropdown">\
					<div class="ftt-trip-el__setting-el" onclick="hideqst('+data["qid"]+', \''+arg+'\');">'+FLang["Delete"]+'</div>\
				</div>\
			</div>\
		</div>';
	}
	
	th = '';
	if ( parseInt(data["canth"]) === 0 ) {
		th = ' thanks_yellow_btn';
	}
	
	th_c = parseInt(data["thanks"]);
	if ( th_c > 0 ) {
		th_c = ' <b>'+th_c+'</b>';
	} else {
		th_c = ''
	}
	
	thanks = '';
	if ( parseInt(data["tip"]) === 1 ) {
		thanks = '<a href="" class="ftt-tile__thanks-btn'+th+'" onclick="proceed_thanks('+data["qid"]+', this);return false;">'+FLang["Thanks"]+th_c+'</a>';
	}
	
	views = '';
	if ( parseInt(data["views"]) ) {
		views = '<div class="ftt-tile__view" alt="'+FLang["Views"]+'" title="'+FLang["Views"]+'">'+data["views"]+'</div>';
	}
	
	reply = '';
	if ( parseInt(data["replys"]) ) {
		var title = "Answers";
		if (thanks.length > 0){
			title = "Comments";
		}
		reply = '<a href="'+data["url"]+'"><div class="ftt-tile__comments" alt="'+FLang[title]+'" title="'+FLang[title]+'">'+data["replys"]+'</div></a>';
	}
	
	return '<div class="ftt-tile ftt-tile_tip" id="qst_'+data["qid"]+'">\
				<div class="ftt-tile__container">\
					<a href="/profile/id'+data["uid"]+'/"><div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data["pic"]+'\')"></div></a>\
					<div class="ftt-tile__name"><a href="/profile/id'+data["uid"]+'/">'+data["name"]+'</a><br>'+city+'</div>\
					<div class="ftt-tile__text" onclick="href_url(\''+data["url"]+'\');return false;">'+data["mes"].replace(r, '<br>')+'</div>\
					<div class="ftt-tile__date" onclick="href_url(\''+data["url"]+'\');return false;">'+data["when"]+'</div>\
					'+views+type+reply+thanks+'\
					<a href="'+data["url"]+'"><div class="ftt-tile__share" alt="'+FLang["Share"]+'" title="'+FLang["Share"]+'"></div></a>\
				</div>'+draw_answers(data.ans, data.tip, data.qid, shortv)+'\
			</div>';
}


function draw_ans(data, tip){
	txt = '<div class="ftt-tile ftt-tile_tip" id="answer_'+data["aid"]+'">\
				<div class="ftt-tile__container">\
					<a href="/profile/id'+data["uid"]+'/"><div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data["pic"]+'\')"></div></a>\
					<div class="ftt-tile__name">'+data["name"]+'</div>\
					<div class="ftt-tile__text">'+data["mes"]+'</div>\
					<div class="ftt-tile__date">'+data["when"]+'</div>\
					<!--a href="#" class="ftt-tile__thanks-btn thanks_yellow_btn">Спасибо <b>3</b></a-->\
					<div class="ftt-tile__setting">\
						<div class="ftt-trip-el__setting js-dropdownToggle" id="prea_'+data["aid"]+'">\
							<div class="ftt-trip-el__setting-control js-dropdownToggleControl" id="edita_'+data["aid"]+'">\
								<div class="ftt-trip-el__setting-dot"></div>\
							</div>\
							<div class="ftt-trip-el__setting-dropdown">\
								<!--div class="ftt-trip-el__setting-el" onclick="alert('+data["aid"]+');">Редактировать</div>\
								<div class="ftt-trip-el__setting-el">Пожаловаться</div!-->';
	if (cuid() === parseInt(data["uid"])) {
		txt += '<div class="ftt-trip-el__setting-el" onclick="hideans('+data["aid"]+');">'+FLang["Delete"]+'</div>';
	}							
								
	txt+='					</div>\
						</div>\
					</div>\
					<!--div class="ftt-tile__share"></div-->\
				</div>\
			</div>';
	return txt;
}

function hideans(aid){
	if (!aid) {return;}
	$("#answer_"+aid).addClass('loadb');
	$.getJSON("/api/v2/hideans?aid="+aid+salt(),
		function(data){
			if (data.status && data.status == 200) {
				$("#answer_"+aid).remove();
			} else {
				exeption(data, o);
			}
		}
	);
}

function hideqst(qid, str){
	if (!qid) {return;}
	$("#qid_"+qid).addClass('loadb');
	$.getJSON("/api/v2/delqst?qid="+qid+salt(),
		function(data){
			if (data.status && data.status == 200) {
				r1 = new RegExp("[ -]","ig");
				r2 = new RegExp("[,]","ig");
				href_url('/feed'+make_url(str, r1, r2));
			} else {
				exeption(data, o);
			}
		}
	);
}

function draw_answers(data, tip, qid, shortv){
	tip = parseInt(tip);
	shortv = parseInt(shortv);
	txt = '<div class="ftt-tile_answer">';
	if (data !== undefined && data.length){
		string = string_ans(data.length);
		if (tip === 1){
			string = string_comment(data.length);
		}
		
		if (shortv === 0) {
			txt += '<div class="ftt-tile_answer_head">\
						<div class="ftt-tile_answer_title">'+data.length+' '+string+'</div>\
					</div>';
			for ( i=0; i < data.length; i++ ){
				txt += draw_ans(data[i])
			}
		}
	}
	var label = "Text_ans";
	if (tip === 1){
		label = "Text_com";
	}
	
	if (shortv === 0) {
		txt += '<div class="ftt-tile__send" id="ans_'+qid+'">\
				<div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+CURRENT_USER["ava"]+'\')"></div>\
				<form action="#" class="ftt-chat-message-form">\
					<textarea class="ftt-chat-message-input" placeholder="'+FLang[label]+'"></textarea>\
					<button class="ftt-chat-message-submit" onclick="post_reply(this, '+qid+');"></button>\
				</form>\
			</div>'
	}
	txt += '</div>';
	return txt;
}

function post_reply(o, qid){
	mes = $(".ftt-chat-message-input").val();
	if (qid <= 0 || mes.length === 0 ) {
		return;
	}
	$(".ftt-chat-message-send").addClass('loadb');
	$(o).attr('disabled', true);
	
	$.post("/api/v2/replysqt", "&qid="+qid+"&mes="+encodeURIComponent(mes)+salt(),
		function(data){
			$(".ftt-chat-message-send").removeClass('loadb');
			$(o).attr('disabled', false);
			if (data.status && data.status == 200 && data.answer.mes.length > 0) {
				$(draw_ans(data.answer)).insertBefore("#ans_"+qid);
				$(".ftt-chat-message-input").val('').focus();
				
				$(document).on('click', '#edita_'+data.answer.aid+' .js-dropdownToggleControl', function(event){
					event.stopPropagation();
					$(this).parent().toggleClass('is-open');
					return false;
				});
				$(document).on('click', function(event){
					$('.js-dropdownToggle').removeClass('is-open');
				});
				
			} else {
					
				if (data.strength) {
					
					$("#warningModal div.ftt-fieldset").html(draw_progress(data));
					$("#warningModal button").html(FLang["Edit_profile"]);
					$("#warningModal div.ftt-modal-tips-mobheader__title, #warningModal div.ftt-modal-tips-title").html(FLang["Add_more_info"]);
					$("#warningModal div.ftt-modal-tips-body p").html(FLang["Why_more_info"]);
					
					$.magnificPopup.open({
						items: {
							src: $("#warningModal"),
							type: 'inline'
						},
						closeBtnInside: true,
						callbacks: {
							open: function() {
								scrollPosition = $(window).scrollTop();
								$('body').addClass('mfp-open');
							},
							close: function(){
								$('body').removeClass('mfp-open');
								$(window).width() <= 767 && $('body, html').scrollTop(scrollPosition);
							}
						}
					});

					$(document).on('click', '.js-modalClose', function(){
						$.magnificPopup.close();
					});
				
				} else {
					exeption(data, o);
				}

			}
		}, "json"
	);
}

function draw_list_tips(data){
	txt = '';
	r = new RegExp("\n","ig");
	for ( i=0; i < data.length; i++ ){
		txt += draw_tip(data[i], r, 1, 1)
	}
	return txt;
}

function draw_list_people(data){

	txt = '';
	if ( data.my !== undefined && data.my["name"] !== undefined ){
		txt +=	'<div class="ftt-tile ftt-tile_full_size is-active-user">\
					<div class="ftt-tile__container">\
						<a href="/profile/id'+cuid()+'/'+data.cid+'/"><div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data.my["pic"]+'\')"><div class="ftt-tile__status-online"></div></div></a>\
						<a href="/profile/id'+cuid()+'/'+data.cid+'/" class="ftt-tile__name">'+data.my["name"]+', '+FLang["Checked_in"]+'</a>\
						<div class="ftt-tile__date">'+data.my["date"]+'</div>\
						<div class="ftt-tile__place ftt-locator-icon">'+data.my["city"]+'</div>\
						<div class="ftt-tile__social">'+make_soc_share(data.my["soc"], 'https://flipthetrip.com/trip/'+data.my["markid"]+'/'+FLang["key"]+'/', 1)+'</div>\
					</div>\
				</div>';
	}
	
	for ( i=0; i < data.res.length; i++ ){
		age = '';
		online = '';
		newmark = '';
		if (parseInt(data.res[i]["age"]) > 0) { age = ', '+data.res[i]["age"];}
		if (parseInt(data.res[i]["online"]) > 0) { online = '<div class="ftt-tile__status-online"></div>'}
		if (parseInt(data.res[i]["new"]) > 0) { newmark = ' is-active-user'; }
		txt +=	'<div class="ftt-tile ftt-tile_full_size'+newmark+'">\
					<div class="ftt-tile__container">\
						<a href="/profile/id'+data.res[i]["uid"]+'/'+data.cid+'/"><div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data.res[i]["pic"]+'\')">'+online+'</div></a>\
						<a href="/profile/id'+data.res[i]["uid"]+'/'+data.cid+'/" class="ftt-tile__name">'+data.res[i]["name"]+age+'</a>\
						<div class="ftt-tile__date">'+data.res[i]["date"]+'</div>\
						<div class="ftt-tile__place ftt-locator-icon">'+data.res[i]["city"]+'</div>\
					</div>\
				</div>';
	}
	
	
	if (data.nearby.length) {
		txt += '<div class="ftt-tile ftt-tile_full_size">\
					<div class="ftt-tile__container" style="background-color:#f0f0f0;">\
						<div class="ftt-tile__name">'+FLang["Nearby_people"]+'</div>\
					</div>\
		</div>';
		for ( i=0; i < data.nearby.length; i++ ){
			age = '';
			online = '';
			if (parseInt(data.nearby[i]["age"]) > 0) { age = ', '+data.nearby[i]["age"];}
			if (parseInt(data.nearby[i]["online"]) > 0) { online = '<div class="ftt-tile__status-online"></div>'}

			txt +=	'<div class="ftt-tile ftt-tile_full_size">\
						<div class="ftt-tile__container">\
							<a href="/profile/id'+data.nearby[i]["uid"]+'/'+data.cid+'/"><div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data.nearby[i]["pic"]+'\')">'+online+'</div></a>\
							<a href="/profile/id'+data.nearby[i]["uid"]+'/'+data.cid+'/" class="ftt-tile__name">'+data.nearby[i]["name"]+age+'</a>\
							<div class="ftt-tile__date">'+data.nearby[i]["date"]+'</div>\
							<div class="ftt-tile__place ftt-locator-icon">'+data.nearby[i]["city"]+'</div>\
						</div>\
					</div>';
		}	
	}
	
	
	
	if (data.res.length == 0 && data.nearby.length == 0) {
		txt += '<div class="ftt-tile ftt-tile_full_size">\
					<div class="ftt-tile__container">\
						<div class="ftt-tile__ava" style="background-image:url(\'/img/icons/ic_no_result.png\')"></div>\
						<div class="ftt-tile__name">'+FLang["Nobody_here"]+'</div>\
					</div>\
		</div>';
	}
	
	return txt;
}

function refresh_list_people(){
	add = '';
	cid = INTERFACE["cid"];
	if (cid > 0){ add = "&cid="+cid }
	$("#container").addClass('loadb');
	$.getJSON("/api/v2/listpro?ppage=50"+add+salt(),
		function(data){
			if (data.status && data.status == 200) {
				$("#people").html(draw_list_people(data));
				$("#container").removeClass('loadb');
				
			} else {
				exeption(data, o);
			}
		}
	);
}

function qstvisit(id){
	$.getJSON("/api/v2/qstvisit?id="+id+salt(),
		function(data){
			if (data.status && data.status == 200) {
		
			}
		}
	);
}

function update_counter(o, cnt){
	var b = $(o).find('b');
	var cur = parseInt($(b).html());
	cur += cnt;
	$(b).html(cur);
}

function proceed_thanks(id, o) {
	$(o).addClass('loadb');
	var method = 'thanks';
	if ( type != undefined ) {
		if ( $(o).hasClass('thanks_yellow_btn') ) { method = 'unthanks'; }
	}
	$.getJSON("/api/v2/"+method+"?id="+id+salt(),
		function(data){
			$(o).removeClass('loadb');
			if (data.status && data.status == 200) {
				var counter = 1;
				if (method === "thanks") {
					$(o).addClass('thanks_yellow_btn');
				} else {
					$(o).removeClass('thanks_yellow_btn');
					counter = -1
				}
				update_counter(o, counter);
			} else {
				exeption(data, o);
			}
		}
	);
}

function save_filter(type, start, end) {
	method = '';
	
	
	if ( type != undefined ) {
		if ( type === 'sex' ) { method = 'save_filter_sex?sex='+start; }
		if ( type === 'age' ) { method = 'save_filter_age?from='+start+'&to='+end; }
	}
	
	if (method.length) {
		$("#filters").addClass('loadb');
		$.getJSON("/api/v2/"+method+salt(),
			function(data){
				if (data.status && data.status == 200) {
					refresh_list_people();
					$("#filters").removeClass('loadb');
				} else {
					exeption(data, o);
				}
			}
		);
		
	}
}

function filters_view(){
	body = $('body');
	filterAside = $('#filters');
	
	if(filterAside.hasClass('is-open')){
		body.removeClass('is-filter-open');
		filterAside.removeClass('is-open');
		$('body, html').scrollTop(scrollPosition);
	}
	else {
		scrollPosition = $(window).scrollTop();
		body.addClass('is-filter-open');
		filterAside.addClass('is-open');
	}
}

function back_filters_view(){
	$('body').removeClass('is-filter-open');
	$('#filters').removeClass('is-open');
	$('body, html').scrollTop(scrollPosition);
}

function exeption(data, o) {
	if (data.id == "0") {
		alert('auth');
	} else {
		error_message(o, data.error);
	}
}

function error_message(o,text) {
	if (text == undefined){text = ''}
	id = 'warn_'+o.id;
	if (! $("#"+id).length){
		$('<div id="'+id+'" class="error"><div>error: '+text+'</div></div>').insertAfter(o).show().fadeOut(5000,function(){$('#'+id).remove();});
	}
}

function fetch_counts(){
	$.getJSON("/api/v2/calc_new?"+salt(),
		function(data){
			if (data.status && data.status == 200) {
				mes = parseInt(data["menu"]["chats"]);
				profile = parseInt(data["menu"]["profile"]);
				trips = parseInt(data["menu"]["trips"]);
				if (mes > 0) {
					if ($("#menu_chat span.ftt-minheader__control-number").length) {
						$("#menu_chat span.ftt-minheader__control-number").html(''+mes);
					} else {
						$("#menu_chat").html('<span class="ftt-minheader__control-number">'+mes+'</span>');
					}
					
					if ($("#menu_chat_burger span.ftt-minheader__burger-number").length) {
						$("#menu_chat_burger span.ftt-minheader__burger-number").html(''+mes);
					} else {
						$("#menu_chat_burger").html('<span class="ftt-minheader__burger-number">'+mes+'</span>');
					}
					
					if ($("#menu_chat_mobile span.ftt-mobmenu__nav-number").length) {
						$("#menu_chat_mobile span.ftt-mobmenu__nav-number").html(''+mes);
					} else {
						$("#menu_chat_mobile").append('<span class="ftt-mobmenu__nav-number">'+mes+'</span>');
					}
					
				} else {
					$("#menu_chat span.ftt-minheader__control-number").remove();
					$("#menu_chat_mobile span.ftt-mobmenu__nav-number").remove();
				}
				
				if (trips > 0) {
					if (! $("#menu_trips span.ftt-minheader__control-number").length) {
						$("#menu_trips").html('<span class="ftt-minheader__control-number"></span>');
					}
					if (! $("#menu_chat_burger span.ftt-minheader__burger-number").length) {
						$("#menu_chat_burger").html('<span class="ftt-minheader__burger-number"></span>');
					} 					
					if (! $("#menu_trips_mobile span.ftt-mobmenu__nav-number").length) {
						$("#menu_trips_mobile").append('<span class="ftt-mobmenu__nav-number"></span>');
					} 
					
				} else {
					$("#menu_trips span.ftt-minheader__control-number").remove();
					$("#menu_trips_mobile span.ftt-mobmenu__nav-number").remove();
				}
				
				if (profile > 0) {
					if (! $("span.ftt-minheader__user-avatar-numbers").length) {
						$("span.ftt-minheader__user-avatar").html('<span class="ftt-minheader__user-avatar-numbers"></span>');
					}
				} else {
					$("span.ftt-minheader__user-avatar-numbers").remove();
				}
				
			} else {
				exeption(data);
			}
		}
	);
}

function megadiv(id) {
	if (! $("#megadiv").length){
		$('<div id="megadiv"></div>').insertAfter("div.ftt-minheader");
		$("#megadiv").click(function(){
			$("#"+id).addClass("hidden");
			$(this).remove();
		});
	}
}

function setsearch(o)  {
	$("#suggest").val(o.innerHTML).focus();
}

function chanhe(obj,id)  {
	if (id == 1){obj.style.backgroundColor='#F7F7F7'}
	else {obj.style.backgroundColor=''}
}


function getpan(o, profile, countrysearch, trips){

	if (! $("#search").hasClass('loadb')){
		$("#search").addClass('loadb');
	}
	
	url = 'suggest';
	if (o.value.length > 1) {
		url = 'search';
		if (countrysearch){
			url += 'con'
		}
	}

	$.getJSON("/api/v2/"+url+"?&keyword="+o.value+"&r=" + (Math.floor(Math.random() * 100000)),
		function(data){
			$("#search").removeClass('loadb');
			if (data.status && data.status == 200) {
				if ((data.res != undefined && data.res.length) || (data.sug != undefined && data.sug.length)) {
					megadiv(o.id+'_div');
					
					txt = '';
					if (data.sug != undefined) {
						txt += '<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);"><span>'+FLang["Recent"]+':</span></div>';
						txt += suggest_list(data.sug, o.value, profile, countrysearch, trips);
					} else {
						txt += suggest_list(data.res, o.value, profile, countrysearch, trips);
					}
					
					$("#"+o.id+'_div').html(txt).removeClass('hidden');
					
				} else {
					$("#"+o.id+'_div').addClass("hidden");
				}
			} else {
				exeption(data, o);
			}
		}
	);
}

function save_cidmark(cid, title){
	if (parseInt(cid)) {
		$("#mark_cid").val(cid);
		$("#hometown_div").html('').addClass('hidden');
		$("#hometown").val(title);
	}
}

function suggest_list (data, kval, profile, countrysearch, trips){
	txt = '';
	r = new RegExp("("+kval+")","ig");
	r1 = new RegExp("[ -]","ig");
    r2 = new RegExp("[,]","ig");
	var base = '';
	if (INTERFACE !== null && INTERFACE !== undefined && INTERFACE["tab"] === "feed") {
		base = '/feed'
	}
	for (i=0;i<data.length;i++){
		val = data[i][0];
		val = val.replace(r,"<b>$1</b>");
		val = val.split(',');
		val = val[0]+((val[1] != undefined) ? '<span>, '+val[1]+((val[2] != undefined) ? ', '+val[2] : '' )+'</span>' : '');
		url = base + make_url(data[i][0], r1, r2);
		
		if (trips){
			txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="save_cidmark('+data[i][1]+', \''+data[i][0]+'\');">'+val+'</div>';
			continue;
		}
		
		if (profile && !countrysearch){
			txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="save_hometown('+data[i][1]+', this, \''+data[i][0]+'\');">'+val+'</div>';
		} else if (profile && countrysearch) {
			txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="save_country('+data[i][1]+', this, \''+data[i][0]+'\');">'+val+'</div>';
		} else {
			txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="save_suggest('+data[i][1]+');href_url(\''+url+'\');">'+val+'</div>';
		}
		
		
	}	
	return txt;
}

function make_url(string, r, r1) {
	s = string.split(', ');
    con = s[s.length-1];
    city = s.slice(0, -1).join();
	return '/'+city.replace(r, '_').replace(r1, '__')+'-'+con.replace(r, '_').replace(r1, '__')+'/';
}

function href_url(url){
	window.location.href = url;
}

function save_suggest(cid) {
	$.post("/api/v2/savesuggest", "cid="+cid+salt(), function(data){}, "json");
}

function logout(o) {
	$(o).addClass('loadb');
	
	if (CURRENT_USER["soc"] === "fb") {
		FB.getLoginStatus(function(response){
			console.log(response);
			if (response && response.status === 'connected') {
			
				FB.getAccessToken();
				FB.logout(function(response) {
					flip_logout(o);
				});
			}
		});
	} else {
		flip_logout(o);
	}
}

function flip_logout(o){
	$.post("/api/v2/exit", salt(), function(data){
		$(o).removeClass('loadb');
		redir_main();
	}, "json");	
}

$.extend(true, $.magnificPopup.defaults, {
	tClose: 'Закрыть (Esc)',
	tLoading: 'Загружается...',
		gallery: {
		tPrev: 'Предыдущая (←)',
		tNext: 'Следующая (→)',
		tCounter: '%curr% из %total%'
	},
	image: {
		tError: '<a href="%url%">Данная картинка</a> не может быть загружена.'
	},
	ajax: {
		tError: '<a href="%url%">Содержимое</a> не может быть загружено.'
	},
	closeMarkup: '<div class="modal-close js-mfp-close"></div>',
	removalDelay: 300,
	closeBtnInside: true
});

/*
function aut(id) {
	$.post("/api/v2/auth", "&key="+id+"&r=" + (Math.floor(Math.random() * 100000)),
		function(data){
			if (data.status && data.status == 200) {
				if (parseInt(data.id) > 0){
					window.location.replace("/");
				} else {
					error_message(o, 'auth error');
				}
			} else {
				error_message(o, data.error);
			}
		}, "json"
	);
}
*/