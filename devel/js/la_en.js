﻿var FLang = {
	key:'en',
	Tip:'Tip',
	Question:'Question',
	Recent:'Recent searches',
	People:'People',
	Feed:'Feed',
	Chekin:'Check-in',
	Showmore:'Show more',
	Showmale:'Only Men',
	Showfemale:'Only Women',
	Showboth:'Men and Women',
	Age:'Age',
	Nobody_here:'Nobody here yet, be the first one to check-in. This will help others find you.',
	Nearby_people:'Travelers in the nearby cities',
	Leaders:'Thanks leaders',
	Recent_feed:'Recent tip and questions',
	Read_more:'Read more',
	Chats:'Chats',
	Showprofile:'Show profile',
	You:'You',
	Text_message:'Write a message',
	mon_0:'January',
	mon_1:'February',
	mon_2:'March',
	mon_3:'April',
	mon_4:'May',
	mon_5:'Jun',
	mon_6:'July',
	mon_7:'August',
	mon_8:'September',			
	mon_9:'October',
	mon_10:'November',
	mon_11:'December',
	Today:'Today',
	Yesterday:'Yesterday',
	Thanks:'Thanks',
	Add:'Add',
	My_tip:'My tip',
	My_qst:'My question',
	Send:'Send',
	Ask_qst:'Ask question',
	Write_tip:'Write a tip',
	Qst_placeholder:'Write text here... Try to be short and clear.',
	Tip_placeholder:'Write text here... Try to be short and clear. Tip should be about how to save time or money.',
	Qst_intro:'Ask locals and travelers.',
	Tip_intro:'Write a tip for travelers. Share your experience.',
	Views:'Views',
	Share:'Share',
	Add_trip:'Add trip',
	My_trip:'My trip',
	Select_dates:'Select date',
	My_trip_intro:'Add place and dates of your future trip. This will help others find you.',
	Checked_in:'You have checked in here',
	Logout:'Logout',
	Hidden_mode:'Hidden mode',
	Settings:'Settings',
	Lives_in:'Lives in',
	Message:'Message',
	About:'About',
	Languages:'Languages',
	Stranu_1:'Country visited',
	Stran_1:'Countries visited',
	Strani_1:'Countries visited',
	Stranu_0:'Country visited',
	Stran_0:'Countries visited',
	Strani_0:'Countries visited',
	Sovet:'Tip',
	Soveta:'Tips',
	Sovetov:'Tips',
	Said_thanks:'Said thanks',
	Dney_with:'Days with FLIPTHETRIP',
	Den_with:'Day with FLIPTHETRIP',
	Dnya_with:'Days with FLIPTHETRIP',
	Instagram_photos:'Photos in Instagram',
	Past_trips:'Recent trips',
	Visited_cons:'Visited countries',
	Upcoming_trip:'Upcoming trip',
	Add_more:'Add more',
	Profile_guests:'Seen your profile',
	Profile_strength:'Profile strength',
	Edit_profile:'Edit profile',
	Make_avatar:'Set as avatar',
	Upload_photo:'Upload photo',
	Saving:'Saving ...',
	Saved:'Saved',
	Birth_date:'Day of birth',
	Hometown:'Hometown',
	Hometown_placeholder:'Which city do you live in?',
	Input_country:'Input country',
	Show_social:'Show link to my',
	Soc_fb:'Facebook',
	Soc_vk:'Vkontakte',
	Remove_instagram:'Remove Instagram',
	Add_instagram:'Add Instagram',
	Add_more_info:'Add more info!',
	Why_more_info:'Fill your profile with info, because nobody responds to empty profiles.',
	Delete:'Delete',
	Trips:'Trips',
	New_users_Search:'New users in search',
	Share_vk:'Share to Vkontakte',
	Share_fb:'Share to Facebook',
	Answers:'Answers',
	Comments:'Comments',
	сomment:'сomment',
	сommenta:'сomments',
	сommentov:'сomments',
	answer:'answer',
	answera:'answers',
	answerov:'answers',
	Text_ans:'Write an answer',
	Text_com:'Write a comment',
	thanked_0:'said thanks to',
	thanked_1:'said thanks to',
	for_tip:'for the tip',
}