﻿function getpan(o){

	if (! $("#search").hasClass('loadb')){
		$("#search").addClass('loadb');
	}

	$.getJSON("/api/search/search?nostat=1&keyword="+o.value+"&r=" + (Math.floor(Math.random() * 100000)),
		function(data){
			$("#search").removeClass('loadb');
			if (data.status && data.status == 200) {
				if (data.res.length){
					
					megadiv(o.id+'_div');
					
					txt='';
					r = new RegExp("("+o.value+")","ig");
					r1 = new RegExp("[ -]","ig");
					r2 = new RegExp("[,]","ig");
					for (i=0;i<data.res.length;i++){
						val = data.res[i][0];
						val = val.replace(r,"<b>$1</b>");
						val = val.split(',');
						val = val[0]+((val[1] != undefined) ? '<span>, '+val[1]+((val[2] != undefined) ? ', '+val[2] : '' )+'</span>' : '');
						url = make_url(data.res[i][0], r1, r2);
						//txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="load_main('+data.res[i][1]+');return false;">'+val+'</div>';
						txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="href_url(\''+url+'\');">'+val+'</div>';
					}

					$("#"+o.id+'_div').html(txt).removeClass('hidden');
					
				} else {
					$("#"+o.id+'_div').addClass("hidden");
				}
			} else {
				error_message(o,data.error);
			}
		}
	);
}

function make_url(string, r, r1) {
	s = string.split(', ');
    con = s[s.length-1];
    city = s.slice(0, -1).join();
	base = '';
	if (url_lang != undefined && url_lang != null) {
		base = url_lang+'/';
	}
	return '/'+base+city.replace(r, '_').replace(r1, '__')+'-'+con.replace(r, '_').replace(r1, '__')+'/';
}

function href_url(url){
	window.location.href = url;
}

function megadiv(id) {
	if (! $("#megadiv").length){
		$('<div id="megadiv"></div>').insertAfter("div.ftt-header");
		$("#megadiv").click(function(){
			$("#"+id).addClass("hidden");
			$(this).remove();
		});
	}
}

function setsearch(o)  {
	$("#suggest").val(o.innerHTML).focus();
}

function chanhe(obj,id)  {
	if (id == 1){obj.style.backgroundColor='#F7F7F7'}
	else {obj.style.backgroundColor=''}
}

function checkposts(){
					
	$("#tips div.ftt-tile__text").each(function(i, obj) {
		if ($(obj)[0].scrollHeight >  $(obj).innerHeight() && $(obj).find('a').length === 0) {
			read_more(obj);
		}
	});

}

function read_more(obj){
	r = new RegExp("<br>","ig");
	r1 = new RegExp("\n","ig");
	txt = $(obj).html().replace(r, "\n");
	$(obj).html(txt);
    var innerText = $(obj).text();
    var textArray = innerText.split('');
    var formatted = '';
    for(var i in textArray) {
        formatted += '<span>' + textArray[i] + '</span>';
    }
    var heightOfContainer = $(obj).height();
    $(obj).html(formatted);
    var clipped = '';
    $(obj).find('span').each(function(){
        if ($(this).position().top < heightOfContainer) {
            clipped += $(this).text();
        } else {
            return false;
        }
    });
	pure = clipped;
	clipped = trim_br(clipped.replace(r1, "<br>").trim());
    clipped += '... <a href="">'+FLang["Read_more"]+'</a>';
    $(obj).html(clipped);
	
	while (check_visible(obj)){
		pure = pure.slice(0,-15);
		$(obj).html(trim_br(pure.replace(r1, "<br>").trim())+'... <a href="" onclick="open_auth();return false;">'+FLang["Read_more"]+'</a>');
	}
}

function trim_br(clipped){
	if (clipped.substr(clipped.length - 4) === "<br>" || clipped.substr(clipped.length - 4) === "<BR>"){
		clipped = clipped.slice(0,-4);
	};
	return clipped;
}

function check_visible(element){
	var invisibleItems = [];
	for(var i=0; i<element.childElementCount; i++){
		if (element.children[i].tagName !== "A") { continue; }
		
	  if (element.children[i].offsetTop + element.children[i].offsetHeight >
		  element.offsetTop + element.offsetHeight ||
		  element.children[i].offsetLeft + element.children[i].offsetWidth >
		  element.offsetLeft + element.offsetWidth ){

			invisibleItems.push(element.children[i].tagName);
		}

	}
	if (invisibleItems.length) { return true }
	return false;
}


function aut(id) {
	$.post("/api/v2/auth", "&key="+id+"&lang="+FLang["key"]+"&web=1&r=" + (Math.floor(Math.random() * 100000)),
		function(data){
			if (data.status && data.status == 200) {
				if (parseInt(data.id) > 0){
					window.location.replace("/");
				} else {
					error_message(o, 'auth error');
				}
			} else {
				error_message(o, data.error);
			}
		}, "json"
	);
}

function open_auth(){
	var authModal = $('#authModal');
	authModal.addClass('is-open');
	$('body, html').animate({
		scrollTop: authModal.offset().top - 40
	}, 300);
}

$(function(){
	var authModal = $('#authModal');

	$(document).on('click', '.js-authModalLink', function(event){
		event.stopPropagation();
		event.preventDefault();
		authModal.addClass('is-open');
		$('body, html').animate({
			scrollTop: authModal.offset().top - 40
		}, 300);
	});

	$(document).on('click', '.js-authModalClose', function(){
		authModal.removeClass('is-open');
	});

	$(document).on('click', function(event){
		if(authModal.has(event.target).length === 0){
		    authModal.removeClass('is-open');
		}
	});
});

function href_url(url){
	window.location.href = url;
}